var lineWidth = 1;
var drawGrid = true;
var drawInternalGrid = true;
var drawPositionLines = false;

var billardWidth = 710;
document.documentElement.style.setProperty('--billard-width', billardWidth + 'px');
var limitSize = billardWidth / (96 * 2);
const LEGEND_POSITION = {
    N: 'NORTH',
    S: 'SOUTH',
    E: 'EAST',
    EE: 'EAST_EAST',
    W: 'WEST',
    WW: 'WEST_WEST',
    WWW: 'WEST_WEST_WEST',
    NE: 'NORTH_EAST',
    NEE: 'NORTH_EAST_EAST',
    NW: 'NORTH_WEST',
    SW: 'SOUTH_WEST',
    SWW: 'SOUTH_WEST_WEST',
    SE: 'SOUTH_EAST',
    SEE: 'SOUTH_EAST_EAST',
    SS: 'SOUTH_SOUTH'
}
const Point = class {
    constructor(secteur, point, satellite, legendPosition, isOnLine) {
        this.secteur = secteur;
        this.point = point;
        this.satellite = satellite;
        this.legendPosition = legendPosition;
        this.isOnLine = isOnLine;
    }
};

const OnLinePoint = class extends Point {
    constructor() {
        super(null, null, null, null, true);
    }
}

var figures = {
    _54: { blanche: new Point('O', 116, 9, LEGEND_POSITION.E), jaune: new Point('L', 142, 7, LEGEND_POSITION.E), rouge: new Point('H', 13, undefined, LEGEND_POSITION.NEE), name: 54 }
    , _61: { blanche: new Point('P', 23, 7, LEGEND_POSITION.N), jaune: new Point('P', 36, 8, LEGEND_POSITION.E), rouge: new Point('O', 6, 8, LEGEND_POSITION.E), name: 61 }
    , _95: { blanche: new Point('I', 55, 8, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 16, 9), lines: [new Point('I', 15, 8)], name: 95 }
    , _78: { blanche: new OnLinePoint(), jaune: new Point('I', 113, 9, LEGEND_POSITION.SE), rouge: new Point('H', 15, undefined, LEGEND_POSITION.NE), lines: [new Point('I', 153, 7)], name: 78 }
    , _9: { blanche: new Point('P', 16, 7, LEGEND_POSITION.W), jaune: new Point('P', 52, 7, LEGEND_POSITION.E), rouge: new Point('A', 114, undefined, LEGEND_POSITION.E), name: '9' }
    , _71: { blanche: new Point('A', 33, 9, LEGEND_POSITION.SW), jaune: new OnLinePoint(), rouge: new Point('A', 25, undefined), lines: [new Point('A', 3, 9)], name: 71 }
    , _93: { blanche: new Point('M', 153, 7, LEGEND_POSITION.E), jaune: new Point('M', 42, undefined, LEGEND_POSITION.EE), rouge: new Point('I', 14, undefined), limit: new Point('O', 53, 7, LEGEND_POSITION.E), name: 93 }
    , _53: { blanche: new Point('F', 35, undefined, LEGEND_POSITION.E), jaune: new Point('G', 34, 8, LEGEND_POSITION.E), rouge: new Point('H', 13, undefined, LEGEND_POSITION.NE), limit: new Point('G', 6, 8, LEGEND_POSITION.E), name: 53 }
    , _4: { blanche: new Point('O', 43, 7, LEGEND_POSITION.E), jaune: new Point('P', 53, 7, LEGEND_POSITION.E), rouge: new Point('P', 16, 9), name: '4' }
    , _6: { blanche: new Point('A', 4, undefined, LEGEND_POSITION.E), jaune: new Point('B', 6, 9, LEGEND_POSITION.WWW), rouge: new Point('B', 16, 9, LEGEND_POSITION.W), limit: new Point('N', 2, undefined, LEGEND_POSITION.E), name: '6' }
    , _72: { blanche: new Point('H', 23, undefined, LEGEND_POSITION.W), jaune: new OnLinePoint(), rouge: new Point('A', 34, undefined, LEGEND_POSITION.SE), lines: [new Point('I', 156, 7)], name: 72 }
    , _2: { blanche: new Point('K', 156, 8, LEGEND_POSITION.WW), jaune: new Point('I', 6, undefined, LEGEND_POSITION.W), rouge: new Point('P', 13, undefined, LEGEND_POSITION.SS), name: '2' }
    , _69: { blanche: new Point('O', 5, 8, LEGEND_POSITION.E), jaune: new Point('N', 14, 7, LEGEND_POSITION.E), rouge: new Point('P', 16, 8, LEGEND_POSITION.E), name: 69 }
    , _66: { blanche: new OnLinePoint(), jaune: new Point('N', 154, 8, LEGEND_POSITION.S), rouge: new Point('I', 14, undefined), lines: [new Point('O', 156, 8)], name: 66 }
    , _10: { blanche: new Point('J', 131, 7, LEGEND_POSITION.E), jaune: new Point('I', 25, undefined, LEGEND_POSITION.E), rouge: new Point('F', 16, undefined, LEGEND_POSITION.E), name: 10 }
    , _24: { blanche: new Point('O', 116, undefined, LEGEND_POSITION.W), jaune: new Point('G', 23, 9, LEGEND_POSITION.E), rouge: new Point('P', 13, 9, LEGEND_POSITION.S), name: 24 }
    , _67: { blanche: new Point('H', 51, 8, LEGEND_POSITION.E), jaune: new Point('G', 6, 9, LEGEND_POSITION.SE), rouge: new Point('G', 16, 8, LEGEND_POSITION.W), name: 67 }
    , _83: { blanche: new Point('O', 13, 9, LEGEND_POSITION.E), jaune: new Point('O', 26, 9, LEGEND_POSITION.E), rouge: new Point('A', 43, 8, LEGEND_POSITION.N), name: 83 }
    , _25: { blanche: new OnLinePoint(), jaune: new Point('I', 156, 7, LEGEND_POSITION.NE), rouge: new Point('I', 155, 7, LEGEND_POSITION.S), lines: [new Point('H', 23, 8), new Point('H', 122, 9)], name: 25 }
    , _33: { blanche: new Point('O', 56, 7, LEGEND_POSITION.W), jaune: new Point('N', 126, 9, LEGEND_POSITION.E), rouge: new Point('P', 13, undefined, LEGEND_POSITION.S), name: 33 }
    , _82: { blanche: new Point('P', 142, 7, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 14, undefined, LEGEND_POSITION.E), limit: new Point('M', 6, 9, LEGEND_POSITION.E), lines: [new Point('P', 52, undefined)], name: 82 }
    , _86: { blanche: new Point('O', 156, 8), jaune: new Point('A', 6, undefined, LEGEND_POSITION.N), rouge: new Point('G', 22, 9, LEGEND_POSITION.NE), name: 86 }
    , _58: { blanche: new Point('A', 23, 9, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('K', 13, 9, LEGEND_POSITION.E), lines: [new Point('A', 3, 9)], name: 58 }
    , _52: { blanche: new Point('F', 13, 9, LEGEND_POSITION.E), jaune: new Point('G', 26, 9, LEGEND_POSITION.E), rouge: new Point('B', 24, 9, LEGEND_POSITION.E), name: 52 }
    , _65: { blanche: new Point('I', 23, 9, LEGEND_POSITION.S), jaune: new OnLinePoint(), rouge: new Point('E', 16, 9, LEGEND_POSITION.E), lines: [new Point('I', 52, 9)], name: 65 }
    , _5: { blanche: new Point('O', 6, 8, LEGEND_POSITION.SE), jaune: new Point('N', 14, 7, LEGEND_POSITION.SE), rouge: new Point('P', 33, undefined, LEGEND_POSITION.E), name: '5' }
    , _99: { blanche: new OnLinePoint(), jaune: new Point('P', 23, 7, LEGEND_POSITION.S), rouge: new Point('P', 12, undefined, LEGEND_POSITION.S), lines: [new Point('P', 21, 7)], name: 99 }
    , _42: { blanche: new Point('L', 152, 8, LEGEND_POSITION.E), jaune: new Point('M', 16, 9, LEGEND_POSITION.E), rouge: new Point('G', 16, 9, LEGEND_POSITION.E), name: 42 }
    , _8: { blanche: new OnLinePoint(), jaune: new Point('H', 12, undefined, LEGEND_POSITION.S), rouge: new Point('A', 14, undefined), limit: new Point('N', 156, 8, LEGEND_POSITION.SW), lines: [new Point('H', 21, undefined)], name: '8' }
    , _80: { blanche: new Point('O', 34, 9, LEGEND_POSITION.E), jaune: new Point('P', 33, undefined, LEGEND_POSITION.N), rouge: new Point('A', 11, 8, LEGEND_POSITION.E), name: 80 }
    , _50: { blanche: new Point('O', 123, undefined, LEGEND_POSITION.E), jaune: new Point('O', 156, 8), rouge: new Point('H', 53, 7, LEGEND_POSITION.E), name: 50 }
    , _63: { blanche: new Point('O', 121, undefined, LEGEND_POSITION.E), jaune: new Point('O', 156, 8), rouge: new Point('C', 16, 9, LEGEND_POSITION.E), name: 63 }
    , _97: { blanche: new Point('I', 35, undefined, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('F', 12, 7, LEGEND_POSITION.E), limit: new Point('I', 51, undefined, LEGEND_POSITION.EE), lines: [new Point('I', 2, undefined)], name: 97 }
    , _41: { blanche: new Point('J', 151, 7, LEGEND_POSITION.E), jaune: new Point('F', 26, 9, LEGEND_POSITION.E), rouge: new Point('A', 34, undefined, LEGEND_POSITION.NE), name: 41 }
    , _92: { blanche: new Point('P', 23, 9, LEGEND_POSITION.S), jaune: new OnLinePoint(), rouge: new Point('A', 14, undefined), limit: new Point('C', 51, 9, LEGEND_POSITION.E), lines: [new Point('P', 26, undefined)], name: 92 }
    , _45: { blanche: new Point('P', 22, 8, LEGEND_POSITION.W), jaune: new Point('P', 104, 7, LEGEND_POSITION.E), rouge: new Point('B', 16, 7, LEGEND_POSITION.W), name: 45 }
    , _1: { blanche: new Point('F', 45, undefined, LEGEND_POSITION.E), jaune: new Point('G', 56, 8, LEGEND_POSITION.E), rouge: new Point('A', 14, undefined), name: '1' }
    , _68: { blanche: new Point('P', 6, 8, LEGEND_POSITION.E), jaune: new Point('H', 6, 9, LEGEND_POSITION.SE), rouge: new Point('H', 16, 8, LEGEND_POSITION.E), limit: new Point('B', 54, 8, LEGEND_POSITION.E), name: 68 }
    , _18: { blanche: new Point('C', 25, undefined, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('I', 14, undefined), limit: new Point('M', 6, 9, LEGEND_POSITION.SE), lines: [new Point('C', 46, 8)], name: 18 }
    , _94: { blanche: new Point('B', 45, 7, LEGEND_POSITION.E), jaune: new Point('C', 146, 9, LEGEND_POSITION.EE), rouge: new Point('H', 13, undefined, LEGEND_POSITION.SEE), name: 94 }
    , _3: { blanche: new Point('O', 122, undefined, LEGEND_POSITION.E), jaune: new Point('O', 13, 9, LEGEND_POSITION.SE), rouge: new Point('P', 32, undefined, LEGEND_POSITION.W), name: '3' }
    , _49: { blanche: new Point('O', 113, undefined, LEGEND_POSITION.E), jaune: new Point('J', 114, 9, LEGEND_POSITION.E), rouge: new Point('A', 14, undefined), name: 49 }
    , _46: { blanche: new Point('O', 146, 9, LEGEND_POSITION.W), jaune: new Point('I', 156, 7, LEGEND_POSITION.SE), rouge: new Point('P', 13, undefined, LEGEND_POSITION.W), name: 46 }
    , _81: { blanche: new Point('E', 36, 9, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 13, 9, LEGEND_POSITION.SS), limit: new Point('I', 156, 7, LEGEND_POSITION.SW), lines: [new Point('D', 143, undefined)], name: 81 }
    , _7: { blanche: new Point('A', 3, 9, LEGEND_POSITION.W), jaune: new Point('G', 6, 9, LEGEND_POSITION.SWW), rouge: new Point('A', 14, 9, LEGEND_POSITION.N), name: '7' }
    , _56: { blanche: new OnLinePoint(), jaune: new Point('C', 6, 9, LEGEND_POSITION.W), rouge: new Point('G', 22, 9, LEGEND_POSITION.NW), limit: new Point('C', 16, 9, LEGEND_POSITION.W), lines: [new Point('B', 2, 9), new Point('B', 12, 9)], name: 56 }
    , _31: { blanche: new Point('B', 13, 8, LEGEND_POSITION.E), jaune: new Point('C', 56, 8, LEGEND_POSITION.E), rouge: new Point('B', 52, 8, LEGEND_POSITION.E), name: 31 }
    , _84: { blanche: new Point('F', 4, 8, LEGEND_POSITION.N), jaune: new Point('G', 16, 9, LEGEND_POSITION.EE), rouge: new Point('I', 14, undefined), limit: new Point('L', 6, 9, LEGEND_POSITION.E), name: 84 }
    , _36: { blanche: new OnLinePoint(), jaune: new Point('D', 156, 8, LEGEND_POSITION.NW), rouge: new Point('A', 15, undefined, LEGEND_POSITION.E), lines: [new Point('D', 56, 8)], name: 36 }
    , _85: { blanche: new Point('A', 4, undefined, LEGEND_POSITION.EE), jaune: new Point('B', 6, 9, LEGEND_POSITION.W), rouge: new Point('B', 16, 9, LEGEND_POSITION.WW), name: 85 }
    , _17: { blanche: new Point('P', 34, 7, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('D', 13, 9, LEGEND_POSITION.E), lines: [new Point('P', 56, undefined)], name: 17 }
    , _22: { blanche: new Point('A', 44, undefined, LEGEND_POSITION.W), jaune: new Point('B', 46, 9, LEGEND_POSITION.SW), rouge: new Point('B', 26, 9, LEGEND_POSITION.E), name: 22 }
    , _44: { blanche: new Point('P', 123, 8, LEGEND_POSITION.E), jaune: new Point('A', 15, undefined, LEGEND_POSITION.SE), rouge: new Point('P', 11, 9, LEGEND_POSITION.W), name: 44 }
    , _70: { blanche: new Point('F', 134, 7, LEGEND_POSITION.E), jaune: new Point('K', 16, 9, LEGEND_POSITION.E), rouge: new Point('P', 32, 9, LEGEND_POSITION.N), limit: new Point('M', 32, 7, LEGEND_POSITION.E), name: 70 }
    , _60: { blanche: new Point('C', 126, 8, LEGEND_POSITION.E), jaune: new Point('A', 25, undefined, LEGEND_POSITION.NW), rouge: new Point('B', 45, undefined, LEGEND_POSITION.E), name: 60 }
    , _90: { blanche: new Point('F', 45, 7, LEGEND_POSITION.E), jaune: new Point('H', 123, undefined, LEGEND_POSITION.N), rouge: new Point('I', 55, 7, LEGEND_POSITION.W), name: 90 }
    , _48: { blanche: new OnLinePoint(), jaune: new Point('I', 113, 9, LEGEND_POSITION.W), rouge: new Point('P', 16, 9), lines: [new Point('I', 153, 7)], name: 48 }
    , _13: { blanche: new Point('M', 151, 8, LEGEND_POSITION.E), jaune: new Point('M', 42, undefined, LEGEND_POSITION.E), rouge: new Point('O', 16, 9, LEGEND_POSITION.E), limit: new Point('N', 55, 8, LEGEND_POSITION.E), name: 13 }
    , _16: { blanche: new Point('A', 44, undefined, LEGEND_POSITION.WW), jaune: new Point('B', 46, 9, LEGEND_POSITION.NW), rouge: new Point('P', 16, 9), limit: new Point('O', 156, 8), name: 16 }
    , _74: { blanche: new Point('A', 4, 8, LEGEND_POSITION.E), jaune: new Point('B', 6, 9, LEGEND_POSITION.WW), rouge: new Point('K', 16, 9, LEGEND_POSITION.EE), name: 74 }
    , _37: { blanche: new OnLinePoint(), jaune: new Point('P', 151, 7, LEGEND_POSITION.E), rouge: new Point('P', 16, 9), limit: new Point('C', 16, 9, LEGEND_POSITION.WW), lines: [new Point('P', 23, 8)], name: 37 }
    , _14: { blanche: new Point('O', 6, 8, LEGEND_POSITION.EE), jaune: new Point('I', 153, 7, LEGEND_POSITION.E), rouge: new Point('P', 16, 9), name: 14 }
    , _15: { blanche: new Point('A', 55, 7, LEGEND_POSITION.NE), jaune: new Point('P', 12, undefined, LEGEND_POSITION.N), rouge: new Point('J', 14, 7, LEGEND_POSITION.E), name: 15 }
    , _55: { blanche: new OnLinePoint(), jaune: new Point('P', 33, undefined, LEGEND_POSITION.EE), rouge: new Point('A', 23, undefined, LEGEND_POSITION.N), lines: [new Point('P', 15, 9)], name: 55 }
    , _76: { blanche: new OnLinePoint(), jaune: new Point('H', 5, 9, LEGEND_POSITION.SE), rouge: new Point('I', 55, 7, LEGEND_POSITION.WW), lines: [new Point('G', 3, 9)], name: 76 }
    , _19: { blanche: new Point('P', 152, 7, LEGEND_POSITION.NE), jaune: new Point('A', 16, 7, LEGEND_POSITION.N), rouge: new Point('M', 13, 9, LEGEND_POSITION.E), name: 19 }
    , _35: { blanche: new Point('G', 6, 9, LEGEND_POSITION.SEE), jaune: new OnLinePoint(), rouge: new Point('A', 15, undefined, LEGEND_POSITION.NE), lines: [new Point('G', 1, undefined), new Point('G', 21, 7)], name: 35 }
    , _89: { blanche: new Point('N', 151, 7, LEGEND_POSITION.E), jaune: new Point('C', 16, 8, LEGEND_POSITION.W), rouge: new Point('I', 14, undefined), name: 89 }
    , _23: { blanche: new Point('K', 156, 8, LEGEND_POSITION.W), jaune: new Point('I', 23, undefined, LEGEND_POSITION.W), rouge: new Point('O', 16, 9, LEGEND_POSITION.EE), name: 23 }
    , _59: { blanche: new Point('O', 56, 8, LEGEND_POSITION.E), jaune: new Point('P', 53, 7, LEGEND_POSITION.E), rouge: new Point('A', 55, 7, LEGEND_POSITION.SE), name: 59 }
    , _40: { blanche: new Point('O', 106, 8, LEGEND_POSITION.E), jaune: new Point('H', 123, 9, LEGEND_POSITION.E), rouge: new Point('G', 13, 8, LEGEND_POSITION.E), name: 40 }
    , _51: { blanche: new Point('P', 22, 9, LEGEND_POSITION.W), jaune: new Point('P', 104, 9, LEGEND_POSITION.E), rouge: new Point('P', 24, undefined, LEGEND_POSITION.S), name: 51 }
    , _98: { blanche: new Point('F', 11, 7, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('I', 14, undefined), lines: [new Point('G', 22, 9)], name: 98 }
    , _21: { blanche: new Point('G', 21, undefined, LEGEND_POSITION.E), jaune: new Point('G', 111, undefined, LEGEND_POSITION.E), rouge: new Point('P', 16, 9), limit: new Point('F', 102, 8, LEGEND_POSITION.E), name: 21 }
    , _29: { blanche: new Point('B', 115, undefined, LEGEND_POSITION.E), jaune: new Point('A', 32, undefined, LEGEND_POSITION.E), rouge: new Point('A', 42, 8, LEGEND_POSITION.E), name: 29 }
    , _38: { blanche: new Point('A', 13, 8, LEGEND_POSITION.E), jaune: new Point('A', 22, 9, LEGEND_POSITION.N), rouge: new Point('A', 33, 9, LEGEND_POSITION.NW), name: 38 }
    , _62: { blanche: new Point('D', 116, 8, LEGEND_POSITION.E), jaune: new Point('M', 156, 8, LEGEND_POSITION.E), rouge: new Point('O', 26, 8, LEGEND_POSITION.E), name: 62 }
    , _27: { blanche: new Point('M', 3, 9, LEGEND_POSITION.E), jaune: new Point('N', 6, 9, LEGEND_POSITION.E), rouge: new Point('A', 11, 9, LEGEND_POSITION.E), name: 27 }
    , _34: { blanche: new Point('I', 14, 7, LEGEND_POSITION.E), jaune: new Point('I', 51, undefined, LEGEND_POSITION.E), rouge: new Point('O', 21, undefined, LEGEND_POSITION.E), name: 34 }
    , _79: { blanche: new Point('A', 114, undefined, LEGEND_POSITION.EE), jaune: new Point('B', 116, 9, LEGEND_POSITION.E), rouge: new Point('B', 6, 8, LEGEND_POSITION.E), limit: new Point('B', 55, undefined, LEGEND_POSITION.E), name: 79 }
    , _64: { blanche: new Point('P', 123, 7, LEGEND_POSITION.W), jaune: new Point('F', 6, 9, LEGEND_POSITION.E), rouge: new Point('H', 13, undefined, LEGEND_POSITION.SE), name: 64 }
    , _57: { blanche: new OnLinePoint(), jaune: new Point('F', 4, undefined, LEGEND_POSITION.E), rouge: new Point('H', 15, undefined, LEGEND_POSITION.NW), lines: [new Point('E', 31, undefined)], name: 57 }
    , _96: { blanche: new OnLinePoint(), jaune: new Point('P', 156, 8, LEGEND_POSITION.E), rouge: new Point('P', 152, 7, LEGEND_POSITION.SE), lines: [new Point('P', 56, 8)], name: 96 }
    , _77: { blanche: new Point('G', 1, 7, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('I', 135, undefined, LEGEND_POSITION.E), limit: new Point('I', 113, 9, LEGEND_POSITION.NE), lines: [new Point('H', 1, 7)], name: 77 }
    , _32: { blanche: new Point('B', 121, undefined, LEGEND_POSITION.E), jaune: new Point('K', 151, 7, LEGEND_POSITION.E), rouge: new Point('K', 16, undefined, LEGEND_POSITION.W), limit: new Point('L', 125, 9, LEGEND_POSITION.E), name: 32 }
    , _87: { blanche: new Point('I', 3, 9, LEGEND_POSITION.E), jaune: new Point('P', 1, undefined, LEGEND_POSITION.E), rouge: new Point('A', 11, 9, LEGEND_POSITION.EE), name: 87 }
    , _47: { blanche: new Point('A', 53, undefined, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('P', 16, 8, LEGEND_POSITION.E), lines: [new Point('A', 41, 7)], name: 47 }
    , _12: { blanche: new Point('I', 143, 9, LEGEND_POSITION.E), jaune: new Point('J', 135, 9, LEGEND_POSITION.E), rouge: new Point('K', 12, undefined, LEGEND_POSITION.E), name: 12 }
    , _26: { blanche: new OnLinePoint(), jaune: new Point('B', 26, 9, LEGEND_POSITION.EE), rouge: new Point('A', 35, 7, LEGEND_POSITION.E), lines: [new Point('A', 51, 9)], name: 26 }
    , _88: { blanche: new Point('C', 6, 8, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('A', 14, undefined), lines: [new Point('C', 22, 7)], name: 88 }
    , _20: { blanche: new Point('I', 24, undefined, LEGEND_POSITION.E), jaune: new Point('I', 21, 7, LEGEND_POSITION.E), rouge: new Point('M', 36, 9, LEGEND_POSITION.E), name: 20 }
    , _28: { blanche: new Point('H', 6, 8, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('A', 14, undefined), lines: [new Point('H', 12, 8)], name: 28 }
    , _91: { blanche: new Point('A', 44, 7, LEGEND_POSITION.N), jaune: new Point('M', 3, 8, LEGEND_POSITION.E), rouge: new Point('M', 23, undefined, LEGEND_POSITION.E), name: 91 }
    , _43: { blanche: new Point('C', 146, 8, LEGEND_POSITION.E), jaune: new Point('E', 3, 9, LEGEND_POSITION.E), rouge: new Point('D', 16, undefined, LEGEND_POSITION.E), name: 43 }
    , _39: { blanche: new Point('C', 146, 9, LEGEND_POSITION.E), jaune: new Point('G', 6, 8, LEGEND_POSITION.E), rouge: new Point('I', 14, undefined), name: 39 }
    , _100: { blanche: new Point('N', 6, 8, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('A', 14, undefined), limit: new Point('O', 156, 8), lines: [new Point('N', 56, 8)], name: 100 }
    , _75: { blanche: new Point('A', 44, 9, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 153, 8, LEGEND_POSITION.E), lines: [new Point('B', 14, 7)], name: 75 }
    , _11: { blanche: new Point('A', 14, undefined), jaune: new Point('B', 26, 8, LEGEND_POSITION.W), rouge: new Point('A', 1, 9, LEGEND_POSITION.E), name: 11 }
    , _73: { blanche: new Point('E', 46, 7, LEGEND_POSITION.E), jaune: new Point('H', 146, 9, LEGEND_POSITION.E), rouge: new Point('H', 111, undefined, LEGEND_POSITION.E), name: 73 }
    , _30: { blanche: new Point('C', 6, 9, LEGEND_POSITION.WW), jaune: new Point('G', 6, 9, LEGEND_POSITION.SW), rouge: new Point('H', 12, 9, LEGEND_POSITION.S), name: 30 }
}

function draw(figure) {
    var canvas = document.getElementById('canvas');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = '#0f7933';
        ctx.fillRect(0, 0, billardWidth, (billardWidth * 2));
        ctx.strokeStyle = '#000';
        ctx.strokeRect(0, 0, billardWidth, (billardWidth * 2));


        if (drawGrid) {
            for (var i = 1; i < 25; i++) {
                if ((i) % 12 == 0 || drawInternalGrid) {
                    ctx.beginPath();
                    ctx.moveTo(i * (billardWidth / 24), 0);
                    ctx.lineTo(i * (billardWidth / 24), (billardWidth * 2));

                    if ((i) % 12 == 0) {
                        ctx.strokeStyle = '#ff0000';
                        ctx.stroke();
                    } else {
                        ctx.strokeStyle = '#000';
                        ctx.stroke();
                    }
                }
            }
            for (var i = 1; i < 49; i++) {
                if ((i) % 6 == 0 || drawInternalGrid) {
                    ctx.beginPath();
                    ctx.moveTo(0, i * ((billardWidth * 2) / 48));
                    ctx.lineTo(billardWidth, i * ((billardWidth * 2) / 48));

                    if ((i) % 6 == 0) {
                        ctx.strokeStyle = '#ff0000';
                        ctx.stroke();
                    } else {
                        ctx.strokeStyle = '#000';
                        ctx.stroke();
                    }
                }
            }
        }

        if (figure){
            drawFigure(ctx, figure);

            var cb = ballToCoord(figure.blanche);
            var cy = ballToCoord(figure.jaune);
            ctx.beginPath();
            ctx.moveTo(cb.x,cb.y);
            ctx.lineTo(cy.x,cy.y);


            ctx.lineTo.apply(ctx,ballToCoordInArray(new Point('A', 5)));
            ctx.lineTo.apply(ctx,ballToCoordInArray(new Point('A', 6,8)));
            ctx.lineTo.apply(ctx,ballToCoordInArray(new Point('K', 4)));
            ctx.lineTo.apply(ctx,ballToCoordInArray(new Point('I', 156,8)));
            ctx.lineTo.apply(ctx,ballToCoordInArray(new Point('G', 6,9)));
            ctx.lineTo.apply(ctx,ballToCoordInArray(figure.rouge));
            ctx.strokeStyle = '#fff';


            ctx.stroke();
        }

        
    }


}



var drawFigure = function (ctx, figure, fillSector, drawOnlyDots) {
    console.log(figure.name);
    ctx.lineWidth = lineWidth;
    var coordWhite, coordYellow;
    if (!figure.blanche.isOnLine) {
        coordWhite = drawBall(ctx, figure.blanche, 'blue', 'white', fillSector, false, false, drawOnlyDots, figure.name, figure.blanche.legendPosition);
    }
    else {
        var coordYellow = drawBall(ctx, figure.jaune, 'green', '#ffc800', fillSector, false, false, drawOnlyDots, figure.name, figure.jaune.legendPosition);
        //find ball coordinate    
        //Calculate distance 
        var lineEndCoords = drawBall(ctx, figure.lines[0], 'blue', 'white', false, true);

        var closestPoint = findClosestPointUntil(coordYellow, lineEndCoords, (billardWidth / 24 + 2));

        if (!drawOnlyDots) {
            ctx.beginPath();
            ctx.strokeStyle = 'black';
            ctx.arc(closestPoint.x, closestPoint.y, (billardWidth / 48), 0, 2 * Math.PI);
            ctx.fillStyle = '#fff';
            ctx.fill();
            ctx.stroke();
        }
    }

    if (!figure.jaune.isOnLine) {
        coordYellow = drawBall(ctx, figure.jaune, 'green', '#ffc800', fillSector, false, drawOnlyDots, drawOnlyDots, figure.name, figure.jaune.legendPosition);
    } else {

        //find ball coordinate    
        //Calculate distance 
        var lineEndCoords = drawBall(ctx, figure.lines[0], 'blue', 'white', false, true);

        //(billardWidth/24 + 2)
        var closestPoint = findClosestPointUntil(coordWhite, lineEndCoords, (billardWidth / 24 + 2));
        /*var quantity = 64;
        var ydiff = lineEndCoords.y - coordWhite.y, xdiff = lineEndCoords.x - coordWhite.x;
        var slope = (lineEndCoords.y - coordWhite.y) / (lineEndCoords.x - coordWhite.x);
        var x, y;
    
        var closestPoint;
        for (var i = 0; i < quantity; i++) {
            y = slope == 0 ? 0 : ydiff * (i / quantity);
            x = slope == 0 ? xdiff * (i / quantity) : y / slope;
            _x = Math.round(x) + coordWhite.x;
            _y = Math.round(y) + coordWhite.y;
            distance = Math.sqrt(Math.pow(_x-coordWhite.x,2) + Math.pow(_y-coordWhite.y,2));
            
            if(distance>(billardWidth/24 + 2))
            {    
                closestPoint = {x:_x,y:_y};
                break;
            }
        }*/
        if (!drawOnlyDots) {
            ctx.beginPath();
            ctx.strokeStyle = 'black';
            ctx.arc(closestPoint.x, closestPoint.y, (billardWidth / 48), 0, 2 * Math.PI);

            ctx.fillStyle = '#ffc800';
            ctx.fill();
            ctx.stroke();
        }


    }
    console.log("White: "+coordWhite);
    console.log("Yellow: "+coordYellow);
    console.log()

    drawBall(ctx, figure.rouge, 'purple', 'red', fillSector, false, drawOnlyDots, drawOnlyDots, figure.name, figure.rouge.legendPosition);
    if (figure.limit) {
        drawBall(ctx, figure.limit, 'green', '#ffc800', fillSector, false, true, true, figure.name, figure.limit.legendPosition);
    }
    if (figure.lines) {
        figure.lines.forEach(function (item) {
            var lineEndCoords = drawBall(ctx, item, 'blue', 'white', false, true);


            ctx.beginPath();
            ctx.setLineDash([3, 3]);
            var realLineEnd;
            if (figure.jaune.isOnLine) {
                ctx.moveTo(coordWhite.x, coordWhite.y);
                realLineEnd = findClosestPointUntil(coordWhite, lineEndCoords, billardWidth / 8) || lineEndCoords;
            } else {
                ctx.moveTo(coordYellow.x, coordYellow.y);
                realLineEnd = findClosestPointUntil(coordYellow, lineEndCoords, billardWidth / 8) || lineEndCoords;
            }

            ctx.lineTo(realLineEnd.x, realLineEnd.y);
            ctx.strokeStyle = '#000';
            ctx.stroke();
            ctx.setLineDash([]);
        });
    }
};
var ballToCoord = function(ball){
    var tmp = ballToCoordInArray(ball);
    return {x:tmp[0],y:tmp[1]};
}
var ballToCoordInArray = function(ball){
    var tmpSecteur = (ball.secteur.toLowerCase().charCodeAt(0) - 'a'.charCodeAt(0));

    var localSecteur = tmpSecteur % 8;
    var isRight = Math.floor(tmpSecteur / 8);


    var coordX, coordY;
    if (!isRight) {

        
        var y = 6 - ball.point % 10;
        var x = Math.floor(ball.point / 10);


        coordY = ((billardWidth * 2) - ((y + 6 * localSecteur) * ((billardWidth * 2) / 48))) - (billardWidth * 2) / 96;
        if (ball.satellite == 8 || ball.satellite == 9) {
            coordY += (billardWidth * 2) / 96;
        }
        if (x > 5) {
            x -= 4;
        }
        coordX = (x * billardWidth / 24) + billardWidth / 48;
        if (ball.satellite == 7 || ball.satellite == 8) {
            coordX += billardWidth / 48;
        }




    } else {
        
        var y = 6 - ball.point % 10;
        var x = Math.floor(ball.point / 10);

        var coordY = ((y + 6 * localSecteur) * ((billardWidth * 2) / 48)) + (billardWidth * 2) / 96;
        if (ball.satellite == 8 || ball.satellite == 9) {
            coordY -= (billardWidth * 2) / 96;
        }
        if (x > 5) {
            x -= 4;
        }
        var coordX = billardWidth - (x * billardWidth / 24) - billardWidth / 48;
        if (ball.satellite == 7 || ball.satellite == 8) {
            coordX -= billardWidth / 48;
        }

    }

    return  [coordX, coordY];
};
var drawBall = function (ctx, ball, color, ballColor, fillSector, calculateOnly, isLimit, isDot, legend, legendPosition) {
    var tmpSecteur = (ball.secteur.toLowerCase().charCodeAt(0) - 'a'.charCodeAt(0));

    var localSecteur = tmpSecteur % 8;
    var isRight = Math.floor(tmpSecteur / 8);


    var coordX, coordY;
    if (!isRight) {

        if (fillSector) {
            ctx.fillStyle = "rgba(80,80,80,0.5)";
            ctx.fillRect(billardWidth * isRight / 2, (billardWidth * 2) - ((localSecteur + 1) * (billardWidth * 2) / 8), billardWidth / 2, (billardWidth * 2) / 8);
        }
        var y = 6 - ball.point % 10;
        var x = Math.floor(ball.point / 10);


        coordY = ((billardWidth * 2) - ((y + 6 * localSecteur) * ((billardWidth * 2) / 48))) - (billardWidth * 2) / 96;
        if (ball.satellite == 8 || ball.satellite == 9) {
            coordY += (billardWidth * 2) / 96;
        }
        if (x > 5) {
            x -= 4;
        }
        coordX = (x * billardWidth / 24) + billardWidth / 48;
        if (ball.satellite == 7 || ball.satellite == 8) {
            coordX += billardWidth / 48;
        }




    } else {
        if (fillSector) {
            ctx.fillStyle = "rgba(80,80,80,0.5)";
            ctx.fillRect(billardWidth * isRight / 2, ((localSecteur) * (billardWidth * 2) / 8), billardWidth / 2, (billardWidth * 2) / 8);
        }
        var y = 6 - ball.point % 10;
        var x = Math.floor(ball.point / 10);

        var coordY = ((y + 6 * localSecteur) * ((billardWidth * 2) / 48)) + (billardWidth * 2) / 96;
        if (ball.satellite == 8 || ball.satellite == 9) {
            coordY -= (billardWidth * 2) / 96;
        }
        if (x > 5) {
            x -= 4;
        }
        var coordX = billardWidth - (x * billardWidth / 24) - billardWidth / 48;
        if (ball.satellite == 7 || ball.satellite == 8) {
            coordX -= billardWidth / 48;
        }

    }


    if (drawPositionLines) {
        ctx.beginPath();
        ctx.moveTo(0, coordY);
        ctx.lineTo(billardWidth, coordY);
        ctx.strokeStyle = color;
        ctx.stroke();


        ctx.beginPath();
        ctx.moveTo(coordX, 0);
        ctx.lineTo(coordX, (billardWidth * 2));
        ctx.strokeStyle = color;
        ctx.stroke();
    }
    if (!calculateOnly) {
        ctx.beginPath();
        ctx.strokeStyle = 'black';

        if (isDot) {
            ctx.arc(coordX, coordY, limitSize / 2, 0, 2 * Math.PI);
            ctx.fillStyle = 'black';
        } else
            if (isLimit) {
                ctx.arc(coordX, coordY, limitSize, 0, 2 * Math.PI);
                ctx.fillStyle = 'black';
            } else {
                ctx.arc(coordX, coordY, (billardWidth / 48), 0, 2 * Math.PI);
                ctx.fillStyle = ballColor;
            }
        ctx.fill();
        ctx.stroke();

        if (legend && legendPosition) {
            ctx.save();
            ctx.translate(coordX, coordY);
            ctx.rotate((isRight ? (-1) : 1) * Math.PI / 2);
            ctx.textAlign = "center";
            ctx.font = billardWidth / 65 + 'px' + ' Consolas'
            //ctx.font = ctx.font.replace(/\d+px/, billardWidth/65+'px');

            switch (legendPosition) {
                case LEGEND_POSITION.N:
                    ctx.fillText(legend, 0, -3);
                    break;
                case LEGEND_POSITION.NE:
                    ctx.fillText(legend, billardWidth / 70, -3);
                    break;
                case LEGEND_POSITION.NEE:
                    ctx.fillText(',' + legend, billardWidth / 30, -3);
                    break;
                case LEGEND_POSITION.NW:
                    ctx.fillText(legend, -billardWidth / 100, -3);
                    break;
                case LEGEND_POSITION.S:
                    ctx.fillText(legend, 0, 2 + billardWidth / 65);
                    break;
                case LEGEND_POSITION.SS:
                    ctx.fillText(legend, 0, (2 + billardWidth * 2 / 65));
                    break;
                case LEGEND_POSITION.SE:
                    ctx.fillText(legend, billardWidth / 70, 2 + billardWidth / 65);
                    break;
                case LEGEND_POSITION.SEE:
                    ctx.fillText(',' + legend, billardWidth / 30, 2 + billardWidth / 65);
                    break;
                case LEGEND_POSITION.SW:
                    ctx.fillText(legend, -billardWidth / 100, 2 + billardWidth / 65);
                    break;
                case LEGEND_POSITION.SWW:
                    ctx.fillText(legend + ',', -billardWidth / 40, 2 + billardWidth / 65);
                    break;
                case LEGEND_POSITION.W:
                    ctx.fillText(legend, -billardWidth / 60, 4);
                    break;
                case LEGEND_POSITION.WW:
                    ctx.fillText(legend + ',', -billardWidth / 28, 4);
                    break;
                case LEGEND_POSITION.WWW:
                    ctx.fillText(legend + ',', -billardWidth / 18, 4);
                    break;
                case LEGEND_POSITION.E:
                    ctx.fillText(legend, billardWidth / 70, 4);
                    break;
                case LEGEND_POSITION.EE:
                    ctx.fillText(',' + legend, billardWidth / 28, 4);
                    break;
                default:
                    break;
            }
            ctx.restore();

            //ctx.fillText(legend, coordX, coordY);
        }
    }
    return { x: coordX, y: coordY };
}


var drawAllDots = function () {
    var canvas = document.getElementById('canvas');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = '#0f7933';
        ctx.fillRect(0, 0, billardWidth, (billardWidth * 2));
        ctx.strokeStyle = '#000';
        ctx.strokeRect(0, 0, billardWidth, (billardWidth * 2));
        if (drawGrid) {
            for (var i = 1; i < 25; i++) {
                if ((i) % 12 == 0 || drawInternalGrid) {
                    ctx.beginPath();
                    ctx.moveTo(i * (billardWidth / 24), 0);
                    ctx.lineTo(i * (billardWidth / 24), (billardWidth * 2));

                    if ((i) % 12 == 0) {
                        ctx.strokeStyle = '#ff0000';
                        ctx.stroke();
                    } else {
                        ctx.strokeStyle = '#000';
                        ctx.stroke();
                    }
                }
            }
            for (var i = 1; i < 49; i++) {
                if ((i) % 6 == 0 || drawInternalGrid) {
                    ctx.beginPath();
                    ctx.moveTo(0, i * ((billardWidth * 2) / 48));
                    ctx.lineTo(billardWidth, i * ((billardWidth * 2) / 48));

                    if ((i) % 6 == 0) {
                        ctx.strokeStyle = '#ff0000';
                        ctx.stroke();
                    } else {
                        ctx.strokeStyle = '#000';
                        ctx.stroke();
                    }
                }
            }
        }

        Object.keys(figures).forEach(function (item) { drawFigure(ctx, figures[item], false, true) })
    }
}


var findClosestPointUntil = function (from, to, minLength) {
    var quantity = 128;
    var ydiff = to.y - from.y, xdiff = to.x - from.x;
    var slope = (to.y - from.y) / (to.x - from.x);
    var x, y;

    var closestPoint;
    for (var i = 0; i < quantity; i++) {
        y = slope == 0 ? 0 : ydiff * (i / quantity);
        x = slope == 0 ? xdiff * (i / quantity) : y / slope;
        _x = Math.round(x) + from.x;
        _y = Math.round(y) + from.y;
        distance = Math.sqrt(Math.pow(_x - from.x, 2) + Math.pow(_y - from.y, 2));

        if (distance > minLength) {
            closestPoint = { x: _x, y: _y };
            return closestPoint;
            break;
        }
    }
}


document.addEventListener('DOMContentLoaded', function () {
    for (let [key, value] of Object.entries(figures)) {
        var li = document.createElement('li');
        li.innerHTML = key;

        li.addEventListener('mouseover',function(){
            draw(value);
        });
        li.addEventListener('click',function(){
            draw(value);
        });
        document.querySelector('#figureList').appendChild(li);    
        console.log(`${key}: ${value}`);
      }

    var myCanvas = document.createElement('canvas');
    myCanvas.setAttribute('width', billardWidth);
    myCanvas.setAttribute('height', billardWidth * 2);
    myCanvas.setAttribute('id', 'canvas');
    document.querySelector('#container').appendChild(myCanvas);
    drawAllDots();

    //draw(figures['_2']);
});
