var lineWidth = 1;
var drawGrid = false;
var drawInternalGrid = false;
var drawPositionLines = false;
var animate = false;
var billardWidth = 960;
var landscapeMode = false;
var clothColor = '#0cb5d1';
var figureLineWidth = 4;
document.documentElement.style.setProperty('--billard-width', billardWidth + 'px');
var limitSize = billardWidth / (96 * 2);

var isLeftHanded = false;

if(isLeftHanded)
    figures=figuresLeftHanded;

function draw(figure) {
    displayDetails(figure);

    var localFigure = new Figure(figure.blanche, figure.jaune, figure.rouge, figure.lines, figure.limit, figure.name);

    if (lastGroup)
        lastGroup.remove();

    lastGroup = cloth.group();
    drawFigureNumber(lastGroup,figure);
    var path = lastGroup.path().attr({ id: 'thePath' }).m(localFigure.getWhitePosition());
    if (figure) {
        drawFigure(lastGroup, figure);
        if(isLeftHanded) {
            switch (figure.name) {
                case 1: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('J', 4, 9)))
                        .L(ballToCoord(new Point('I', 106)))
                        .L(ballToCoord(new Point('F', 1)))
                        .L(localFigure.getRedPosition());
                    lastGroup.path()
                        .m(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('I', 146)))
                        .L(ballToCoord(new Point('B', 2)))
                        .L(ballToCoord(new Point('A', 46,7)))
                        .L(ballToCoord(new Point('M', 6, 9)))
                        .L(ballToCoord(new Point('H', 101,7)))
                        .L(ballToCoord(new Point('G', 1)))
                        .L(localFigure.getRedPosition())
                        .fill('none')
                        .stroke({
                            color: 'yellow',
                            width: figureLineWidth
                        });
                    var zoneRappelSize = (billardWidth / 24) * 9 * 2;
                    lastGroup.circle(zoneRappelSize)
                        .fill('rgba(255,255,0,0.3)')
                        .move(billardWidth -zoneRappelSize / 2, billardWidth * 2 - zoneRappelSize / 2);
                    break;
                case 2: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('P', 5,9)))
                        .L(ballToCoord(new Point('P', 21)))
                        .L(ballToCoord(new Point('E', 6)))
                        .L(ballToCoord(new Point('I', 136)))
                        .L(ballToCoord(new Point('J', 2)))
                        .L(localFigure.getRedPosition());
                    break;
                case 3: 
                    path
                        .L(ballToCoord(figure.jaune))
                        .L(ballToCoord(new Point('B', 4)))
                        .Q(ballToCoord(new Point('A', 101)), ballToCoord(new Point('A', 5)))
                        .L(ballToCoord(new Point('A', 16)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 4: 
                    path
                        .L(ballToCoord(figure.jaune))
                        .L(ballToCoord(new Point('A', 96)))
                        .Q(ballToCoord(new Point('B', 36, 9)), ballToCoord(new Point('A', 6, 7)))
                        .L(ballToCoord(new Point('A', 4)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 5: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('D', 6)))
                        .Q(ballToCoord(new Point('F', 36,9)), ballToCoord(new Point('B', 2)))
                        .L(ballToCoord(new Point('A', 16, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 6: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('M', 56)), ballToCoord(new Point('C', 4)))
                        .L(ballToCoord(new Point('P', 51, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 7: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('D', 13, 9)), ballToCoord(new Point('F', 6, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 8: 
                    path
                        .Q(ballToCoord(new Point('B', 1)), localFigure.getRedPosition());
                    break;
                case 9: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('A', 106,7)))
                    .Q(ballToCoord(new Point('P', 153, 9)), ballToCoord(new Point('P', 31)))
                    .L(ballToCoord(new Point('P', 2)))
                    .L(ballToCoord(new Point('P', 11)))
                    .L(localFigure.getRedPosition());
                    break;
                case 10: 
                    path
                        .L(ballToCoord(figure.jaune))
                        .L(ballToCoord(new Point('H', 11, 7)))
                        .Q(ballToCoord(new Point('H', 36,9)), ballToCoord(new Point('H', 2)))
                        .L(ballToCoord(new Point('H', 2)))
                        .L(ballToCoord(new Point('H', 1, 7)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 11: 
                    var tmpRed = localFigure.getRedPosition();
                    tmpRed.x -= billardWidth / 24 / 5;
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('O', 4)))
                        .Q(ballToCoord(new Point('N', 44)), ballToCoord(new Point('P', 1, 7)))
                        .L(ballToCoord(new Point('P', 4)))
                        .L(tmpRed);
                    break;
                case 12: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('G', 56)), ballToCoord(new Point('G', 6, 9)))
                        .Q(ballToCoord(new Point('K', 124)), ballToCoord(new Point('I', 5)))
                        .L(ballToCoord(new Point('I', 16)))
                        .L(localFigure.getRedPosition());
                    break;
                case 13: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('C', 36, 9)), ballToCoord(new Point('P', 3)))
                        .L(ballToCoord(new Point('P', 31)))
                        .L(localFigure.getRedPosition());
                    break;
                case 14: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('I', 51)), ballToCoord(new Point('J', 1)))
                        .L(ballToCoord(new Point('A', 106)))
                        .L(localFigure.getRedPosition());
                    break;
                case 15: 
                    path
                        .L(ballToCoord(figure.jaune))
                        .L(ballToCoord(new Point('A', 16, 7)))
                        .Q(ballToCoord(new Point('D', 131, 9)), ballToCoord(new Point('L', 6)))
                        .L(ballToCoord(new Point('H', 121)))
                        .L(ballToCoord(new Point('G', 1)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 16: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('E', 156, 7)), ballToCoord(new Point('A', 46)))
                        .L(localFigure.getRedPosition());
                    break;
                case 17: 
                    path
                        .Q(ballToCoord(new Point('B', 36)), ballToCoord(new Point('B', 3, 9)))
                        .Q(ballToCoord(new Point('D', 126)), ballToCoord(new Point('A', 3, 9)))
                        .L(ballToCoord(new Point('A', 36)))
                        .L(localFigure.getRedPosition());
                    break;
                case 18: 
                    path
                        .Q(ballToCoord(new Point('M', 156)), ballToCoord(new Point('E', 4, 9)))
                        .Q(ballToCoord(new Point('D', 151, 7)), ballToCoord(new Point('P', 21)))
                        .L(ballToCoord(new Point('P', 2)))
                        .L(localFigure.getRedPosition());
                    break;
                case 19: 
                    path
                        .L(ballToCoord(new Point('P', 31)))
                        .L(ballToCoord(new Point('P', 11, 8)))
                        .Q(ballToCoord(new Point('P', 13)), ballToCoord(new Point('P', 4)))	
                        .Q(ballToCoord(new Point('M', 55, 9)), ballToCoord(new Point('I', 5)))	
                        .L(ballToCoord(new Point('I', 6, 7)))	
                        .L(localFigure.getRedPosition());
                    break;
                case 20: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('F', 156, 9)), ballToCoord(new Point('M', 6)))
                        .Q(ballToCoord(new Point('M', 152, 9)), ballToCoord(new Point('D', 5)))
                        .Q(ballToCoord(new Point('M', 151, 9)), ballToCoord(new Point('M', 4)))
                        .L(localFigure.getRedPosition());
                    break;
                case 21: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('K', 131, 7)), ballToCoord(new Point('L', 3, 9)))
                        .L(ballToCoord(new Point('A', 46)))
                        .L(localFigure.getRedPosition());
                    break;
                case 22: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('N', 11, 7)), ballToCoord(new Point('P', 4)))
                        .L(ballToCoord(new Point('P', 1, 7)))
                        .L(ballToCoord(new Point('P', 2)))
                        .L(localFigure.getRedPosition());
                    break;
                case 23: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('M', 3, 9)))
                        .L(ballToCoord(new Point('A', 146)))
                        .L(localFigure.getRedPosition());
                    break;
                case 24: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(localFigure.getRedPosition());
                    break;
                case 25: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('I', 156, 9)))
                        .Q(ballToCoord(new Point('I', 111)), ballToCoord(new Point('H', 3, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 26: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('P', 6)))
                        .Q(ballToCoord(new Point('P', 104)), ballToCoord(new Point('P', 1, 9)))
                        .L(ballToCoord(new Point('P', 11)))
                        .L(localFigure.getRedPosition());
                    break;
                case 27: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('D', 11, 9)), ballToCoord(new Point('E', 1, 9)))
                        .Q(ballToCoord(new Point('G', 13, 9)), ballToCoord(new Point('H', 3, 9)))
                        .L(ballToCoord(new Point('H', 1, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 28: 
                    path
                        .Q(ballToCoord(new Point('I', 152)), ballToCoord(new Point('F', 2, 9)))
                        .Q(ballToCoord(new Point('L', 154)), ballToCoord(new Point('P', 101)))
                        .L(ballToCoord(new Point('O', 3, 9)))
                        .L(ballToCoord(new Point('H', 31, 7)))
                        .L(ballToCoord(new Point('H', 4)))
                        .L(localFigure.getRedPosition());
                    break;
                case 29: 
                    path
                        .L(ballToCoord(new Point('J', 6)))
                        .L(ballToCoord(new Point('I', 26)))
                        .L(ballToCoord(new Point('A', 4)))
                        .L(ballToCoord(new Point('A', 16)))
                        .L(ballToCoord(new Point('L', 6)))
                        .L(ballToCoord(new Point('H', 101)))
                        .L(ballToCoord(new Point('G', 3, 9)))
                        .L(ballToCoord(new Point('P', 6)))
                        .L(ballToCoord(new Point('P', 21)))
                        .L(findMiddle(ballToCoord(figure.jaune), ballToCoord(figure.rouge)));
                    break;
                case 30: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('I', 56)), ballToCoord(new Point('H', 21)))
                        .L(ballToCoord(new Point('H', 1, 9)))
                        .L(ballToCoord(new Point('H', 151)))
                        .L(localFigure.getRedPosition());
                    break;

                case 31: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('C', 134)), ballToCoord(new Point('P', 11)))
                        .L(ballToCoord(new Point('P', 2)))
                        .L(localFigure.getRedPosition());
                    break;
                case 32: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('F', 56)), ballToCoord(new Point('E', 2)))
                        .Q(ballToCoord(new Point('N', 145, 8)), ballToCoord(new Point('P', 31)))
                        .L(ballToCoord(new Point('P', 3, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 33: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(localFigure.getRedPosition());
                    lastGroup
                        .path()
                        .m(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('I', 26, 7)))
                        .L(ballToCoord(new Point('J', 5)))
                        .L(ballToCoord(new Point('A', 126)))
                        .L(ballToCoord(new Point('C', 4)))
                        .L(ballToCoord(new Point('I', 116)))
                        .L(ballToCoord(new Point('K', 5, 9)))
                        .L(localFigure.getRedPosition())
                        .fill('none')
                        .stroke({
                            color: 'yellow',
                            width: figureLineWidth
                        });
                    var zoneRappelSize = (billardWidth / 24) * 9 * 2;
                    lastGroup
                        .circle(zoneRappelSize)
                        .fill('rgba(255,255,0,0.3)')
                        .move( - zoneRappelSize / 2, billardWidth * 2 - zoneRappelSize / 2) ;
                    break;
                case 34: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('N', 1)))
                        .Q(ballToCoord(new Point('M', 56)), ballToCoord(new Point('I', 4)))
                        .L(ballToCoord(new Point('I', 6, 7)))
                        .L(ballToCoord(new Point('I', 5)))
                        .L(localFigure.getRedPosition());
                    break;
                case 35: 
                    path
                        .Q(ballToCoord(new Point('J', 25)), localFigure.getRedPosition());
                    break;
                case 36: 
                    path
                        .Q(ballToCoord(new Point('M', 101, 9)), ballToCoord(new Point('A', 1)))
                        .L(ballToCoord(new Point('A', 56)))
                        .L(ballToCoord(new Point('N', 1)))
                        .L(localFigure.getRedPosition());
                    break;
                case 37: 
                    path
                        .Q(ballToCoord(new Point('O', 156)), ballToCoord(new Point('N', 2)))
                        .Q(ballToCoord(new Point('N', 153)), ballToCoord(new Point('A', 106)))
                        .L(localFigure.getRedPosition());
                    break;
                case 38: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('P', 2)))
                        .L(ballToCoord(new Point('P', 11)))
                        .Q(ballToCoord(new Point('P', 23, 8)), ballToCoord(new Point('P', 1, 9)))
                        .L(ballToCoord(new Point('P', 1, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 39: 
                    path
                        .L(ballToCoord(new Point('K', 5, 9)))
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('C', 6,9)))
                        .L(ballToCoord(new Point('A', 156)))
                        .L(ballToCoord(new Point('O', 6)))
                        .L(localFigure.getRedPosition());
                    break;
                case 40: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('I', 5)))
                        .L(ballToCoord(new Point('H', 41)))
                        .L(ballToCoord(new Point('H', 1, 9)))
                        .L(ballToCoord(new Point('H', 11, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 41: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('L', 5)))
                        .Q(ballToCoord(new Point('L', 51)), ballToCoord(new Point('O', 3, 9)))
                        .L(ballToCoord(new Point('P', 11, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 42: 
                    path
                        .L(ballToCoord(figure.jaune))
                        .L(ballToCoord(new Point('E', 6)))
                        .Q(ballToCoord(new Point('F', 131)), ballToCoord(new Point('H', 2)))
                        .L(ballToCoord(new Point('H', 11)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 43: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('L', 3)))
                        .L(ballToCoord(new Point('A', 4)))
                        .L(ballToCoord(new Point('A', 26)))
                        .L(localFigure.getRedPosition());
                    break;
                case 44: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('P', 11, 7)))
                        .Q(ballToCoord(new Point('E', 153)), ballToCoord(new Point('F', 2)))
                        .L(ballToCoord(new Point('H', 151, 7)))
                        .L(ballToCoord(new Point('J', 1, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 45: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('A', 134)), ballToCoord(new Point('A', 156)))
                        .Q(ballToCoord(new Point('P', 34)), ballToCoord(new Point('A', 5, 9)))
                        .L(ballToCoord(new Point('A', 16, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 46: 
                    path
                        .L(ballToCoord(new Point('I', 156)))
                        .L(ballToCoord(new Point('P', 3)))
                        .L(ballToCoord(new Point('P', 11)))
                        .L(ballToCoord(new Point('F', 3, 9)))
                        .L(ballToCoord(new Point('H', 151, 7)))
                        .L(ballToCoord(new Point('J', 1, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 47: 
                    path
                        .Q(ballToCoord(new Point('P', 23, 9)), ballToCoord(new Point('P', 1, 9)))
                        .L(ballToCoord(new Point('P', 1, 7)))
                        .Q(ballToCoord(new Point('O', 121, 7)), ballToCoord(new Point('A', 126)))
                        .L(localFigure.getRedPosition());
                    break;
                case 48: 
                    path
                        .Q(ballToCoord(new Point('E', 56)), ballToCoord(new Point('C', 3)))
                        .Q(ballToCoord(new Point('C', 36)), ballToCoord(new Point('A', 26)))
                        .L(ballToCoord(new Point('A', 2, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 49: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('I', 132)), ballToCoord(new Point('I', 4, 9)))
                    .L(ballToCoord(new Point('I', 26, 7)))
                    .L(ballToCoord(new Point('G', 5)))
                    .L(localFigure.getRedPosition());
                    break;
                case 50: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('D', 124, 7)), ballToCoord(new Point('E', 5, 9)))
                        .L(ballToCoord(new Point('I', 5)))
                        .L(ballToCoord(new Point('I', 16)))
                        .L(localFigure.getRedPosition());
                    break;

                case 51: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('A', 134)), ballToCoord(new Point('A', 156)))
                        .Q(ballToCoord(new Point('P', 36)), ballToCoord(new Point('A', 26)))
                        .L(ballToCoord(new Point('A', 4, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 52: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('B', 1)))
                        .Q(ballToCoord(new Point('A', 56, 9)), ballToCoord(new Point('H', 4)))
                        .L(ballToCoord(new Point('H', 1, 7)))
                        .L(ballToCoord(new Point('H', 2)))
                        .L(localFigure.getRedPosition());
                    break;
                case 53: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('J', 33)), ballToCoord(new Point('J', 2, 9)))
                        .Q(ballToCoord(new Point('J', 103)), ballToCoord(new Point('A', 26, 7)))
                        .L(ballToCoord(new Point('A', 3)))
                        .L(localFigure.getRedPosition());
                    break;
                case 54: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('L', 51)), ballToCoord(new Point('M', 4)))
                        .Q(ballToCoord(new Point('N', 141, 9)), ballToCoord(new Point('A', 136)))
                        .L(ballToCoord(new Point('B', 1, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 55: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('A', 46, 7)))
                        .Q(ballToCoord(new Point('A', 52)), ballToCoord(new Point('P', 31)))
                        .L(ballToCoord(new Point('P', 2)))
                        .L(localFigure.getRedPosition());
                    break;
                case 56: 
                    path
                        .Q(ballToCoord(new Point('O', 41, 9)), ballToCoord(new Point('I', 1, 9)))
                        .L(ballToCoord(new Point('I', 6, 7)))
                        .L(ballToCoord(new Point('I', 4, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 57: 
                    path
                        .Q(ballToCoord(new Point('J', 111, 9)), ballToCoord(new Point('I', 156)))
                        .Q(ballToCoord(new Point('E', 52, 9)), ballToCoord(new Point('O', 4)))
                        .L(ballToCoord(new Point('P', 111)))
                        .L(ballToCoord(new Point('C', 3, 9)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 58: 
                    path
                        .Q(ballToCoord(new Point('O', 24)), ballToCoord(new Point('O', 6, 9)))
                        .Q(ballToCoord(new Point('N', 56)), ballToCoord(new Point('P', 3)))
                        .L(ballToCoord(new Point('P', 11)))
                        .L(localFigure.getRedPosition());
                    break;
                case 59: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('A', 106, 7)))
                        .Q(ballToCoord(new Point('O', 121)), ballToCoord(new Point('P', 1, 9)))
                        .L(ballToCoord(new Point('P', 21)))
                        .L(localFigure.getRedPosition());
                    break;
                case 60: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('P', 11, 7)))
                        .L(ballToCoord(new Point('P', 2, 9)))
                        .Q(ballToCoord(new Point('O', 101)), ballToCoord(new Point('P', 21)))
                        .L(ballToCoord(new Point('P', 3)))
                        .L(localFigure.getRedPosition());
                    break;
                case 61:
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('E', 106, 9)), ballToCoord(new Point('C', 4, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 62: 
                    path
                        .L(ballToCoord(figure.jaune))
                        .Q(ballToCoord(new Point('D', 31, 9)), ballToCoord(new Point('A', 1)))
                        .L(ballToCoord(new Point('A', 16)))
                        .L(ballToCoord(new Point('A', 3)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 63: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('N', 31, 9)), ballToCoord(new Point('P', 3)))
                        .L(ballToCoord(new Point('P', 11)))
                        .L(ballToCoord(new Point('O', 1, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 64: 
                    path
                    .L(localFigure.getYellowPosition())
                    .L({
                        x: billardWidth,
                        y: ballToCoord(new Point('L', 6))
                            .y
                    }) // WARN FORCE x=billardWidth	
                    .Q(ballToCoord(new Point('N', 156, 8)), ballToCoord(new Point('A', 26)))
                    .L(ballToCoord(new Point('A', 3, 9)))	
                    .L(localFigure.getRedPosition());
                    break;
                case 65: 
                    path
                        .Q(ballToCoord(new Point('O', 26)), ballToCoord(new Point('N', 3, 9)))
                        .Q(ballToCoord(new Point('K', 34)), ballToCoord(new Point('I', 4)))
                        .L(ballToCoord(new Point('I', 16)))
                        .L(localFigure.getRedPosition());
                    break;
                case 66: 
                    path
                        .Q(ballToCoord(new Point('N', 54)), ballToCoord(new Point('P', 21)))
                        .L(ballToCoord(new Point('P', 4)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 67: 
                    path
                        .Q(ballToCoord(new Point('M', 36, 9)), findMiddle(ballToCoord(figure.jaune), ballToCoord(figure.rouge)));
                    break;
                case 68: 
                    path
                        .L(ballToCoord(new Point('P', 115)))
                        .Q(ballToCoord(new Point('P', 4, 9)), findMiddle(ballToCoord(figure.jaune), ballToCoord(figure.rouge)));
                    break;
                case 69: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('C', 1)))
                        .Q(ballToCoord(new Point('E', 42, 7)), ballToCoord(new Point('A', 2, 9)))
                        .L(ballToCoord(new Point('A', 6, 7)))
                        .L(ballToCoord(new Point('A', 5)))
                        .L(localFigure.getRedPosition());
                    break;
                case 70: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('F', 1)))
                        .Q(ballToCoord(new Point('D', 143, 9)), ballToCoord(new Point('B', 3, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 71: 
                    path
                        .Q(ballToCoord(new Point('O', 24)), ballToCoord(new Point('O', 6, 9)))
                        .Q(ballToCoord(new Point('M', 24)), ballToCoord(new Point('M', 6, 9)))
                        .Q(ballToCoord(new Point('K', 34)), ballToCoord(new Point('O', 2, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 72: 
                    path
                        .Q(ballToCoord(new Point('I', 25)), ballToCoord(new Point('I', 16)))
                        .Q(ballToCoord(new Point('I', 15)), ballToCoord(new Point('I', 4, 9)))
                        .Q(ballToCoord(new Point('J', 26, 8)), ballToCoord(new Point('J', 1, 9)))
                        .Q(ballToCoord(new Point('L', 35, 8)), ballToCoord(new Point('N', 5)))
                        .L(localFigure.getRedPosition());
                    break;
                case 73: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('H', 1, 9)))
                        .L(ballToCoord(new Point('H', 11, 7)))
                        .Q(ballToCoord(new Point('I', 154)), ballToCoord(new Point('I', 5, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 74: 
                    path
                        .L(ballToCoord(new Point('P', 5)))
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('B', 135, 9)), ballToCoord(new Point('H', 3)))
                        .L(ballToCoord(new Point('H', 11)))
                        .L(ballToCoord(new Point('H', 5)))
                        .L(localFigure.getRedPosition());
                    break;
                case 75: 
                    path
                        .Q(ballToCoord(new Point('P', 12)), ballToCoord(new Point('P', 1, 9)))
                        .L(ballToCoord(new Point('P', 21)))
                        .Q(ballToCoord(new Point('A', 154, 9)), ballToCoord(new Point('A', 5, 9)))
                        .L(ballToCoord(new Point('A', 16)))
                        .L(localFigure.getRedPosition());
                    break;
                case 76: 
                    path
                        .Q(ballToCoord(new Point('I', 31)), ballToCoord(new Point('I', 56)))
                        .Q(ballToCoord(new Point('J', 146, 9)), ballToCoord(new Point('H', 2)))
                        .L(ballToCoord(new Point('H', 21, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 77: 
                    path
                        .Q(ballToCoord(new Point('H', 4, 9)), localFigure.getRedPosition());
                    break;
                case 78: 
                    path
                        .Q(ballToCoord(new Point('E', 56)), ballToCoord(new Point('C', 3)))
                        .Q(ballToCoord(new Point('B', 56)), ballToCoord(new Point('A', 26)))
                        .L(ballToCoord(new Point('A', 4)))
                        .L(localFigure.getRedPosition());
                    break;
                case 79: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('N', 34)), ballToCoord(new Point('P', 11)))
                        .L(ballToCoord(new Point('P', 4)))
                        .L(localFigure.getRedPosition());
                    break;
                case 80: 
                    path
                        .L(ballToCoord(figure.jaune))
                        .L(ballToCoord(new Point('A', 5, 9)))
                        .L(ballToCoord(new Point('A', 16)))
                        .Q(ballToCoord(new Point('A', 131, 9)), ballToCoord(new Point('P', 111)))
                        .L(ballToCoord(new Point('P', 4)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 81: 
                    path
                        .Q(ballToCoord(new Point('F', 132)), ballToCoord(new Point('H', 141, 7)))
                        .Q(ballToCoord(new Point('F', 132)), ballToCoord(new Point('L', 3, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 82: 
                    path
                        .Q(ballToCoord(new Point('D', 114)), ballToCoord(new Point('E', 4, 9)))
                        .Q(ballToCoord(new Point('F', 51)), localFigure.getRedPosition());
                    break;
                case 83: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('C', 5, 9)))
                        .Q(ballToCoord(new Point('D', 114)), ballToCoord(new Point('P', 2, 9)))
                        .L(ballToCoord(new Point('P', 11, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 84: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('G', 136)), ballToCoord(new Point('F', 5)))
                        .Q(ballToCoord(new Point('E', 156, 9)), ballToCoord(new Point('P', 41)))
                        .L(ballToCoord(new Point('P', 5)))
                        .L(localFigure.getRedPosition());
                    break;
                case 85: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('E', 134)), ballToCoord(new Point('H', 31)))
                        .L(ballToCoord(new Point('H', 4)))
                        .L(ballToCoord(new Point('P', 41)))
                        .L(localFigure.getRedPosition());
                    break;
                case 86: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('G', 13, 9)), ballToCoord(new Point('F', 3, 9)))
                        .Q(ballToCoord(new Point('C', 16, 9)), ballToCoord(new Point('B', 6, 9)))
                        .L(ballToCoord(new Point('A', 6, 7)))
                        .L(ballToCoord(new Point('A', 4)))
                        .L(localFigure.getRedPosition());
                    break;
                case 87: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('B', 13, 9)), ballToCoord(new Point('D', 6, 9)))
                        .Q(ballToCoord(new Point('G', 16, 9)), ballToCoord(new Point('H', 6, 9)))
                        .L(ballToCoord(new Point('H', 1, 7)))
                        .L(localFigure.getRedPosition());
                    break;
                case 88: 
                    path
                        .Q(ballToCoord(new Point('B', 131)), ballToCoord(new Point('A', 5)))
                        .L(ballToCoord(new Point('A', 16)))
                        .Q(ballToCoord(new Point('P', 156, 8)), ballToCoord(new Point('P', 6)))
                        .Q(ballToCoord(new Point('C', 136, 8)), ballToCoord(new Point('B', 4, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 89: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('O', 13, 9)), ballToCoord(new Point('O', 3)))
                        .Q(ballToCoord(new Point('P', 44, 9)), ballToCoord(new Point('P', 21)))
                        .L(ballToCoord(new Point('P', 3, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 90: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('I', 44)), ballToCoord(new Point('I', 26)))
                        .L(ballToCoord(new Point('I', 5)))
                        .Q(ballToCoord(new Point('H', 156, 9)), ballToCoord(new Point('H', 3)))
                        .L(localFigure.getRedPosition());
                    break;
                case 91: 
                    path
                        .Q(ballToCoord(new Point('O', 34)), ballToCoord(new Point('O', 5, 9)))
                        .Q(ballToCoord(new Point('N', 104, 9)), ballToCoord(new Point('P', 2, 9)))
                        .L(ballToCoord(new Point('P', 11)))
                        .L(ballToCoord(new Point('D', 5, 9)))
                        .L(findMiddle(ballToCoord(figure.jaune), ballToCoord(figure.rouge)));
                    break;
                case 92: 
                    var curveInflexionPoint = ballToCoord(new Point('O', 3, 9));
                    curveInflexionPoint.x *= 1.55; //WARN force it because it's outside the billiard
                    path	
                        .Q(curveInflexionPoint, ballToCoord(new Point('H', 101)))	
                        .L(ballToCoord(new Point('H', 6)))	
                        .L(localFigure.getRedPosition());
                    break;
                case 93: 
                    path
                        .L(ballToCoord(figure.jaune))
                        .Q(ballToCoord(new Point('B', 26)), ballToCoord(new Point('A', 126)))
                        .L(ballToCoord(new Point('O', 6)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 94: 
                    path
                        .L(localFigure.getYellowPosition())
                        .Q(ballToCoord(new Point('B', 101)), ballToCoord(new Point('A', 26)))
                        .L(ballToCoord(new Point('A', 4, 9)))
                        .L(localFigure.getRedPosition());
                    lastGroup.path()
                        .m(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('H', 4, 9)))
                        .L(ballToCoord(new Point('H', 31)))
                        .L(ballToCoord(new Point('M', 2)))
                        .L(ballToCoord(new Point('A', 146)))
                        .L(ballToCoord(new Point('B', 4, 9)))
                        .L(localFigure.getRedPosition())
                        .fill('none')
                        .stroke({
                            color: 'yellow',
                            width: figureLineWidth
                        });
                    var zoneRappelSize = (billardWidth / 24) * 9 * 2;
                    lastGroup.circle(zoneRappelSize)
                        .fill('rgba(255,255,0,0.3)')
                        .move(billardWidth -zoneRappelSize / 2, -zoneRappelSize / 2) ;
                    break;
                case 95: 
                    path
                        .Q(ballToCoord(new Point('G', 53)), ballToCoord(new Point('K', 4, 9)))
                        .L(ballToCoord(new Point('A', 116)))
                        .L(localFigure.getRedPosition());
                    break;
                case 96: 
                    path
                        .Q(ballToCoord(new Point('A', 154, 9)), ballToCoord(new Point('A', 16)))
                        .L(ballToCoord(new Point('A', 5, 9)))
                        .L(ballToCoord(new Point('A', 36)))
                        .L(localFigure.getRedPosition());
                    break;
                case 97: 
                    path
                        .Q(ballToCoord(new Point('G', 156)), ballToCoord(new Point('H', 3)))
                        .L(ballToCoord(new Point('H', 21)))
                        .L(localFigure.getRedPosition());
                    break;
                case 98: 
                    path
                        .Q(ballToCoord(new Point('F', 151)), ballToCoord(new Point('F', 4)))
                        .Q(ballToCoord(new Point('K', 144, 9)), ballToCoord(new Point('L', 6)))
                        .Q(ballToCoord(new Point('L', 154, 9)), ballToCoord(new Point('C', 6, 9)))
                        .L(ballToCoord(new Point('A', 156, 7)))
                        .L(ballToCoord(new Point('O', 6, 9)))
                        .L(localFigure.getRedPosition());
                    break;
                case 99: 
                    path.
                    Q(ballToCoord(new Point('A', 45)), ballToCoord(new Point('A', 106)))
                    .Q(ballToCoord(new Point('A', 124, 9)), ballToCoord(new Point('A', 156)))
                    .Q(ballToCoord(new Point('P', 54)), ballToCoord(new Point('A', 46)))
                    .L(localFigure.getRedPosition());
                    break;
                case 100: 
                    path.
                    Q(ballToCoord(new Point('A', 156)), ballToCoord(new Point('N', 3, 9)))
                    .L(ballToCoord(new Point('H', 51)))
                    .L(ballToCoord(new Point('G', 1)))
                    .L(localFigure.getRedPosition());
                    break;
                default:
                    console.log(`figure ${figure.name} not found`);
            }
        } else {
            switch (figure.name) {
                case 1: 
                    if(isLeftHanded){
                        path
                            .L(localFigure.getYellowPosition())
                            .L(ballToCoord(new Point('J', 4, 9)))
                            .L(ballToCoord(new Point('I', 106)))
                            .L(ballToCoord(new Point('F', 1)))
                            .L(localFigure.getRedPosition());
                        lastGroup.path()
                            .m(localFigure.getYellowPosition())
                            .L(ballToCoord(new Point('I', 146)))
                            .L(ballToCoord(new Point('B', 2)))
                            .L(ballToCoord(new Point('A', 46,7)))
                            .L(ballToCoord(new Point('M', 6, 9)))
                            .L(ballToCoord(new Point('H', 101,7)))
                            .L(ballToCoord(new Point('G', 1)))
                            .L(localFigure.getRedPosition())
                            .fill('none')
                            .stroke({
                                color: 'yellow',
                                width: figureLineWidth
                            });
                        var zoneRappelSize = (billardWidth / 24) * 9 * 2;
                        lastGroup.circle(zoneRappelSize)
                            .fill('rgba(255,255,0,0.3)')
                            .move(billardWidth -zoneRappelSize / 2, billardWidth * 2 - zoneRappelSize / 2);
                    } else {
                        path
                            .L(localFigure.getYellowPosition())
                            .L(ballToCoord(new Point('G', 2, 9)))
                            .L(ballToCoord(new Point('H', 101)))
                            .L(ballToCoord(new Point('K', 6)))
                            .L(localFigure.getRedPosition());
                        lastGroup.path()
                            .m(localFigure.getYellowPosition())
                            .L(ballToCoord(new Point('H', 141)))
                            .L(ballToCoord(new Point('O', 5)))
                            .L(ballToCoord(new Point('P', 51)))
                            .L(ballToCoord(new Point('E', 6, 9)))
                            .L(ballToCoord(new Point('I', 56)))
                            .L(ballToCoord(new Point('J', 6)))
                            .L(localFigure.getRedPosition())
                            .fill('none')
                            .stroke({
                                color: 'yellow',
                                width: figureLineWidth
                            });
                        var zoneRappelSize = (billardWidth / 24) * 9 * 2;
                        lastGroup.circle(zoneRappelSize)
                            .fill('rgba(255,255,0,0.3)')
                            .move(-zoneRappelSize / 2, billardWidth * 2 - zoneRappelSize / 2);
                    }
                    break;
                case 2: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('A', 1, 9)))
                        .L(ballToCoord(new Point('A', 26)))
                        .L(ballToCoord(new Point('L', 1)))
                        .L(ballToCoord(new Point('H', 131)))
                        .L(ballToCoord(new Point('G', 5)))
                        .L(localFigure.getRedPosition());
                    break;
                case 3: 
                    path.
                    L(ballToCoord(figure.jaune))
                    .L(ballToCoord(new Point('O', 3)))
                    .Q(ballToCoord(new Point('P', 106)), ballToCoord(new Point('P', 2)))
                    .L(ballToCoord(new Point('P', 2)))
                    .L(ballToCoord(new Point('P', 11)))
                    .L(ballToCoord(figure.rouge));
                    break;
                case 4: 
                    path.
                    L(ballToCoord(figure.jaune))
                    .L(ballToCoord(new Point('P', 91)))
                    .Q(ballToCoord(new Point('P', 36, 9)), ballToCoord(new Point('P', 1, 7)))
                    .L(ballToCoord(new Point('P', 1, 7)))
                    .L(ballToCoord(new Point('P', 3)))
                    .L(ballToCoord(figure.rouge));
                    break;
                case 5: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('M', 1)))
                    .Q(ballToCoord(new Point('L', 36)), ballToCoord(new Point('O', 4, 9)))
                    .L(ballToCoord(new Point('P', 11, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 6: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('D', 51, 9)), ballToCoord(new Point('N', 3)))
                    .L(ballToCoord(new Point('A', 56, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 7: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('E', 13, 9)), ballToCoord(new Point('D', 6, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 8: 
                    path.
                    Q(ballToCoord(new Point('O', 6, 9)), localFigure.getRedPosition());
                ;
                    break;
                case 9: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('P', 101, 7)))
                    .Q(ballToCoord(new Point('P', 153, 9)), ballToCoord(new Point('A', 36)))
                    .L(ballToCoord(new Point('A', 5)))
                    .L(ballToCoord(new Point('A', 16)))
                    .L(localFigure.getRedPosition());
                    break;
                case 10: 
                    path.
                    L(ballToCoord(figure.jaune))
                    .L(ballToCoord(new Point('I', 16, 7)))
                    .Q(ballToCoord(new Point('J', 36, 9)), ballToCoord(new Point('I', 5)))
                    .L(ballToCoord(new Point('I', 5)))
                    .L(ballToCoord(new Point('I', 6, 7)))
                    .L(ballToCoord(figure.rouge));
                    break;
                case 11: 
                    var tmpRed = localFigure.getRedPosition();
                tmpRed.x += billardWidth / 24 / 5;
                path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('B', 3, 9)))
                    .Q(ballToCoord(new Point('C', 43)), ballToCoord(new Point('A', 6, 7)))
                    .L(ballToCoord(new Point('A', 3)))
                    .L(tmpRed) ;
                    break;
                case 12: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('J', 51)), ballToCoord(new Point('K', 6, 9)))
                    .Q(ballToCoord(new Point('F', 123)), ballToCoord(new Point('H', 2)))
                    .L(ballToCoord(new Point('H', 11)))
                    .L(localFigure.getRedPosition());
                    break;
                case 13: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('N', 31, 9)), ballToCoord(new Point('A', 4)))
                    .L(ballToCoord(new Point('A', 36)))
                    .L(localFigure.getRedPosition());
                    break;
                case 14: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('H', 56)), ballToCoord(new Point('G', 6)))
                    .L(ballToCoord(new Point('P', 101)))
                    .L(localFigure.getRedPosition());
                    break;
                case 15: 
                    path.
                    L(ballToCoord(figure.jaune))
                    .L(ballToCoord(new Point('P', 11, 7)))
                    .Q(ballToCoord(new Point('M', 136, 9)), ballToCoord(new Point('E', 1)))
                    .L(ballToCoord(new Point('E', 1)))
                    .L(ballToCoord(new Point('I', 126)))
                    .L(ballToCoord(new Point('J', 6)))
                    .L(ballToCoord(figure.rouge));
                    break;
                case 16: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('E', 156, 7)), ballToCoord(new Point('P', 41)))
                    .L(localFigure.getRedPosition());
                    break;
                case 17: 
                    path.
                    Q(ballToCoord(new Point('O', 31)), ballToCoord(new Point('O', 3, 9)))
                    .Q(ballToCoord(new Point('M', 121)), ballToCoord(new Point('P', 3, 9)))
                    .L(ballToCoord(new Point('P', 31)))
                    .L(localFigure.getRedPosition());
                    break;
                case 18: 
                    path.
                    Q(ballToCoord(new Point('D', 151)), ballToCoord(new Point('L', 2, 9)))
                    .Q(ballToCoord(new Point('D', 151, 7)), ballToCoord(new Point('A', 26)))
                    .L(ballToCoord(new Point('A', 5)))
                    .L(localFigure.getRedPosition());
                    break;
                case 19: 
                    path.
                    L(ballToCoord(new Point('A', 36)))
                    .L(ballToCoord(new Point('A', 15, 8))) //.L(localFigure.getYellowPosition())	
                    .Q(ballToCoord(new Point('A', 14)), ballToCoord(new Point('A', 3)))	
                    .Q(ballToCoord(new Point('D', 52, 9)), ballToCoord(new Point('H', 2)))	
                    .L(ballToCoord(new Point('H', 1, 7)))	
                    .L(localFigure.getRedPosition());
                    break;
                case 20: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('F', 156, 9)), ballToCoord(new Point('D', 1)))
                    .Q(ballToCoord(new Point('M', 152, 9)), ballToCoord(new Point('M', 2)))
                    .Q(ballToCoord(new Point('M', 151, 9)), ballToCoord(new Point('D', 3)))
                    .L(localFigure.getRedPosition());
                    break;
                case 21: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('F', 136, 7)), ballToCoord(new Point('E', 3, 9)))
                    .L(ballToCoord(new Point('P', 41)))
                    .L(localFigure.getRedPosition());
                    break;
                case 22: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('C', 16, 7)), ballToCoord(new Point('A', 3)))
                    .L(ballToCoord(new Point('A', 3)))
                    .L(ballToCoord(new Point('A', 6, 7)))
                    .L(ballToCoord(new Point('A', 5)))
                    .L(localFigure.getRedPosition());
                    break;
                case 23: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('D', 4, 9)))
                    .L(ballToCoord(new Point('P', 141)))
                    .L(localFigure.getRedPosition());
                    break;
                case 24: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(localFigure.getRedPosition());
                    break;
                case 25: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('H', 150, 9)))
                    .Q(ballToCoord(new Point('H', 116)), ballToCoord(new Point('I', 3, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 26: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('A', 1)))
                    .Q(ballToCoord(new Point('A', 103)), ballToCoord(new Point('A', 5, 9)))
                    .L(ballToCoord(new Point('A', 16)))
                    .L(localFigure.getRedPosition());
                    break;
                case 27: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('M', 16, 9)), ballToCoord(new Point('L', 6, 9)))
                    .Q(ballToCoord(new Point('J', 13, 9)), ballToCoord(new Point('I', 4, 9)))
                    .L(ballToCoord(new Point('I', 6, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 28: 
                    path.
                    Q(ballToCoord(new Point('H', 154)), ballToCoord(new Point('K', 5, 9)))
                    .Q(ballToCoord(new Point('E', 153)), ballToCoord(new Point('A', 106)))
                    .L(ballToCoord(new Point('B', 4, 9)))
                    .L(ballToCoord(new Point('I', 36, 7)))
                    .L(ballToCoord(new Point('I', 3)))
                    .L(localFigure.getRedPosition());
                    break;
                case 29: 
                    path.
                    L(ballToCoord(new Point('G', 1)))
                    .L(ballToCoord(new Point('H', 21)))
                    .L(ballToCoord(new Point('P', 3)))
                    .L(ballToCoord(new Point('P', 11)))
                    .L(ballToCoord(new Point('E', 1)))
                    .L(ballToCoord(new Point('I', 106)))
                    .L(ballToCoord(new Point('J', 3, 9)))
                    .L(ballToCoord(new Point('A', 1)))
                    .L(ballToCoord(new Point('A', 26)))
                    .L(findMiddle(ballToCoord(figure.jaune), ballToCoord(figure.rouge)));
                    break;
                case 30: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('H', 51)), ballToCoord(new Point('I', 26)))
                    .L(ballToCoord(new Point('I', 5, 9)))
                    .L(ballToCoord(new Point('I', 156)))
                    .L(localFigure.getRedPosition());
                    break;
                case 31: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('N', 133)), ballToCoord(new Point('A', 16)))
                    .L(ballToCoord(new Point('A', 5)))
                    .L(localFigure.getRedPosition());
                    break;
                case 32: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('K', 51)), ballToCoord(new Point('L', 5)))
                    .Q(ballToCoord(new Point('C', 141, 8)), ballToCoord(new Point('A', 36)))
                    .L(ballToCoord(new Point('A', 3, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 33: 
                    path
                        .L(localFigure.getYellowPosition())
                        .L(localFigure.getRedPosition())
                    lastGroup.path()
                        .m(localFigure.getYellowPosition())
                        .L(ballToCoord(new Point('H', 21, 7)))
                        .L(ballToCoord(new Point('G', 2)))
                        .L(ballToCoord(new Point('P', 121)))
                        .L(ballToCoord(new Point('N', 3)))
                        .L(ballToCoord(new Point('H', 111)))
                        .L(ballToCoord(new Point('F', 1, 9)))
                        .L(localFigure.getRedPosition())
                        .fill('none')
                        .stroke({
                            color: 'yellow',
                            width: figureLineWidth
                        });
                    var zoneRappelSize = (billardWidth / 24) * 9 * 2;
                    lastGroup.circle(zoneRappelSize)
                        .fill('rgba(255,255,0,0.3)')
                        .move(billardWidth - zoneRappelSize / 2, billardWidth * 2 - zoneRappelSize / 2) ;
                    break;
                case 34: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('K', 6)))
                    .Q(ballToCoord(new Point('L', 51)), ballToCoord(new Point('P', 3)))
                    .L(ballToCoord(new Point('P', 1, 7)))
                    .L(ballToCoord(new Point('P', 2)))
                    .L(localFigure.getRedPosition());
                    break;
                case 35: 
                    path.
                    Q(ballToCoord(new Point('G', 22)), localFigure.getRedPosition());
                    break;
                case 36: 
                    path.
                    Q(ballToCoord(new Point('L', 106, 9)), ballToCoord(new Point('H', 6)))
                    .L(ballToCoord(new Point('H', 51)))
                    .L(ballToCoord(new Point('K', 6)))
                    .L(localFigure.getRedPosition());
                    break;
                case 37: 
                    path.
                    Q(ballToCoord(new Point('B', 151)), ballToCoord(new Point('C', 5)))
                    .Q(ballToCoord(new Point('C', 154)), ballToCoord(new Point('P', 101)))
                    .L(localFigure.getRedPosition());
                    break;
                case 38: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('A', 5)))
                    .L(ballToCoord(new Point('A', 16)))
                    .Q(ballToCoord(new Point('A', 23, 8)), ballToCoord(new Point('A', 5, 9)))
                    .L(ballToCoord(new Point('A', 6, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 39: 
                    path.
                    L(ballToCoord(new Point('F', 1, 9)))
                    .L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('O', 6, 9)))
                    .L(ballToCoord(new Point('P', 151)))
                    .L(ballToCoord(new Point('B', 1)))
                    .L(localFigure.getRedPosition());
                    break;
                case 40: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('H', 2)))
                    .L(ballToCoord(new Point('I', 46)))
                    .L(ballToCoord(new Point('I', 5, 9)))
                    .L(ballToCoord(new Point('I', 16, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 41: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('E', 2)))
                    .Q(ballToCoord(new Point('E', 56)), ballToCoord(new Point('B', 3, 9)))
                    .L(ballToCoord(new Point('A', 16, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 42: 
                    path.
                    L(ballToCoord(figure.jaune))
                    .L(ballToCoord(new Point('L', 1)))
                    .Q(ballToCoord(new Point('K', 136)), ballToCoord(new Point('I', 5)))
                    .L(ballToCoord(new Point('I', 5)))
                    .L(ballToCoord(new Point('I', 16)))
                    .L(ballToCoord(figure.rouge));
                    break;
                case 43: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('E', 4)))
                    .L(ballToCoord(new Point('P', 3)))
                    .L(ballToCoord(new Point('P', 21)))
                    .L(localFigure.getRedPosition());
                    break;
                case 44: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('A', 16, 7)))
                    .Q(ballToCoord(new Point('L', 154)), ballToCoord(new Point('K', 5)))
                    .L(ballToCoord(new Point('I', 156, 7)))
                    .L(ballToCoord(new Point('G', 6, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 45: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('P', 133)), ballToCoord(new Point('P', 151)))
                    .Q(ballToCoord(new Point('A', 33)), ballToCoord(new Point('P', 1, 9)))
                    .L(ballToCoord(new Point('P', 11, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 46: 
                    path.
                    L(ballToCoord(new Point('H', 151)))
                    .L(ballToCoord(new Point('A', 4)))
                    .L(ballToCoord(new Point('A', 16)))
                    .L(ballToCoord(new Point('K', 3, 9)))
                    .L(ballToCoord(new Point('I', 156, 7)))
                    .L(ballToCoord(new Point('G', 6, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 47: 
                    path.
                    Q(ballToCoord(new Point('A', 23, 9)), ballToCoord(new Point('A', 5, 9)))
                    .L(ballToCoord(new Point('A', 6, 7)))
                    .Q(ballToCoord(new Point('B', 126, 7)), ballToCoord(new Point('P', 121)))
                    .L(localFigure.getRedPosition());
                    break;
                case 48: 
                    path.
                    Q(ballToCoord(new Point('L', 51)), ballToCoord(new Point('N', 4)))
                    .Q(ballToCoord(new Point('N', 31)), ballToCoord(new Point('P', 21)))
                    .L(ballToCoord(new Point('P', 4, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 49: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('H', 135)), ballToCoord(new Point('H', 2, 9)))
                    .L(ballToCoord(new Point('H', 21, 7)))
                    .L(ballToCoord(new Point('J', 2)))
                    .L(localFigure.getRedPosition());
                    break;
                case 50: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('M', 123, 7)), ballToCoord(new Point('L', 1, 9)))
                    .L(ballToCoord(new Point('H', 2)))
                    .L(ballToCoord(new Point('H', 11)))
                    .L(localFigure.getRedPosition());
                    break;
                case 51: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('P', 133)), ballToCoord(new Point('P', 151)))
                    .Q(ballToCoord(new Point('A', 31)), ballToCoord(new Point('P', 21)))
                    .L(ballToCoord(new Point('P', 2, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 52: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('G', 6)))
                    .Q(ballToCoord(new Point('H', 51, 9)), ballToCoord(new Point('A', 3)))
                    .L(ballToCoord(new Point('A', 6, 7)))
                    .L(ballToCoord(new Point('A', 5)))
                    .L(localFigure.getRedPosition());
                    break;
                case 53: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('G', 34)), ballToCoord(new Point('G', 4, 9)))
                    .Q(ballToCoord(new Point('G', 104)), ballToCoord(new Point('P', 21, 7)))
                    .L(ballToCoord(new Point('P', 4)))
                    .L(localFigure.getRedPosition());
                    break;
                case 54: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('E', 56)), ballToCoord(new Point('D', 3)))
                    .Q(ballToCoord(new Point('C', 146, 9)), ballToCoord(new Point('P', 131)))
                    .L(ballToCoord(new Point('O', 6, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 55: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('P', 41, 7)))
                    .Q(ballToCoord(new Point('P', 55)), ballToCoord(new Point('A', 36)))
                    .L(ballToCoord(new Point('A', 5)))
                    .L(localFigure.getRedPosition());
                    break;
                case 56: 
                    path.
                    Q(ballToCoord(new Point('B', 46, 9)), ballToCoord(new Point('H', 6, 9)))
                    .L(ballToCoord(new Point('H', 1, 7)))
                    .L(ballToCoord(new Point('H', 2, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 57: 
                    path.
                    Q(ballToCoord(new Point('G', 116, 9)), ballToCoord(new Point('H', 151)))
                    .Q(ballToCoord(new Point('L', 55, 9)), ballToCoord(new Point('B', 3)))
                    .L(ballToCoord(new Point('A', 116)))
                    .L(ballToCoord(new Point('N', 3, 9)))
                    .L(ballToCoord(figure.rouge));
                    break;
                case 58: 
                    path.
                    Q(ballToCoord(new Point('B', 23)), ballToCoord(new Point('C', 6, 9)))
                    .Q(ballToCoord(new Point('C', 51)), ballToCoord(new Point('A', 4)))
                    .L(ballToCoord(new Point('A', 16)))
                    .L(localFigure.getRedPosition());
                    break;
                case 59: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('P', 101, 7)))
                    .Q(ballToCoord(new Point('B', 126)), ballToCoord(new Point('A', 5, 9)))
                    .L(ballToCoord(new Point('A', 26)))
                    .L(localFigure.getRedPosition());
                    break;
                case 60: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('A', 16, 7)))
                    .L(ballToCoord(new Point('A', 4, 9)))
                    .Q(ballToCoord(new Point('B', 106)), ballToCoord(new Point('A', 26)))
                    .L(ballToCoord(new Point('A', 4)))
                    .L(localFigure.getRedPosition());
                    break;
                case 61: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('L', 101, 9)), ballToCoord(new Point('N', 2, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 62: 
                    path.
                    L(ballToCoord(figure.jaune))
                    .Q(ballToCoord(new Point('M', 36, 9)), ballToCoord(new Point('P', 6)))
                    .L(ballToCoord(new Point('P', 6)))
                    .L(ballToCoord(new Point('P', 11)))
                    .L(ballToCoord(new Point('P', 4)))
                    .L(ballToCoord(figure.rouge));
                    break;
                case 63: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('C', 36, 9)), ballToCoord(new Point('A', 4)))
                    .L(ballToCoord(new Point('A', 16)))
                    .L(ballToCoord(new Point('B', 5, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 64: 
                    path
                    .L(localFigure.getYellowPosition())
                    .L({
                        x: 0,
                        y: ballToCoord(new Point('E', 1))
                            .y
                    }) // WARN FORCE x=0	
                    .Q(ballToCoord(new Point('C', 151, 8)), ballToCoord(new Point('P', 21)))	
                    .L(ballToCoord(new Point('P', 3, 9)))	
                    .L(localFigure.getRedPosition());
                    break;
                case 65: 
                    path.
                        Q(ballToCoord(new Point('J', 21)), ballToCoord(new Point('K', 3, 9)))
                        .Q(ballToCoord(new Point('N', 33)), ballToCoord(new Point('P', 3)))
                        .L(ballToCoord(new Point('P', 11)))
                        .L(localFigure.getRedPosition());
                    break;
                case 66: 
                    path.
                        Q(ballToCoord(new Point('C', 53)), ballToCoord(new Point('A', 26)))
                        .L(ballToCoord(new Point('A', 3)))
                        .L(ballToCoord(figure.rouge));
                    break;
                case 67: 
                    path.
                    Q(ballToCoord(new Point('E', 36, 9)), findMiddle(ballToCoord(figure.jaune), ballToCoord(figure.rouge)));
                    break;
                case 68: 
                    path.
                    L(ballToCoord(new Point('A', 112)))
                    .Q(ballToCoord(new Point('A', 2, 9)), findMiddle(ballToCoord(figure.jaune), ballToCoord(figure.rouge)));
                    break;
                case 69: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('N', 6)))
                    .Q(ballToCoord(new Point('L', 45, 7)), ballToCoord(new Point('P', 4, 9)))
                    .L(ballToCoord(new Point('P', 1, 7)))
                    .L(ballToCoord(new Point('P', 2)))
                    .L(localFigure.getRedPosition());
                    break;
                case 70: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('K', 6)))
                    .Q(ballToCoord(new Point('M', 143, 9)), ballToCoord(new Point('O', 3, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 71: 
                    path.
                    Q(ballToCoord(new Point('B', 23)), ballToCoord(new Point('C', 6, 9)))
                    .Q(ballToCoord(new Point('D', 23)), ballToCoord(new Point('E', 6, 9)))
                    .Q(ballToCoord(new Point('F', 33)), ballToCoord(new Point('B', 4, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 72: 
                    path.
                    Q(ballToCoord(new Point('H', 22)), ballToCoord(new Point('H', 11)))
                    .Q(ballToCoord(new Point('H', 12)), ballToCoord(new Point('H', 2, 9)))
                    .Q(ballToCoord(new Point('G', 21, 8)), ballToCoord(new Point('G', 5, 9)))
                    .Q(ballToCoord(new Point('E', 32, 8)), ballToCoord(new Point('C', 2)))
                    .L(localFigure.getRedPosition());
                    break;
                case 73: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('I', 5, 9)))
                    .L(ballToCoord(new Point('I', 16, 7)))
                    .Q(ballToCoord(new Point('H', 153)), ballToCoord(new Point('H', 1, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 74: 
                    path.
                    L(ballToCoord(new Point('A', 2)))
                    .L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('O', 131, 9)), ballToCoord(new Point('I', 4)))
                    .L(ballToCoord(new Point('I', 16)))
                    .L(ballToCoord(new Point('I', 2)))
                    .L(localFigure.getRedPosition());
                    break;
                case 74: 
                    path.
                    L(ballToCoord(new Point('A', 2)))
                    .L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('O', 131, 9)), ballToCoord(new Point('I', 4)))
                    .L(ballToCoord(new Point('I', 16)))
                    .L(ballToCoord(new Point('I', 2)));
                    break;
                case 75: 
                    path.
                    Q(ballToCoord(new Point('A', 15)), ballToCoord(new Point('A', 5, 9)))
                    .L(ballToCoord(new Point('A', 26)))
                    .Q(ballToCoord(new Point('P', 152, 9)), ballToCoord(new Point('P', 1, 9)))
                    .L(ballToCoord(new Point('P', 11)))
                    .L(localFigure.getRedPosition());
                    break;
                case 76: 
                    path.
                    Q(ballToCoord(new Point('H', 36)), ballToCoord(new Point('H', 51)))
                    .Q(ballToCoord(new Point('H', 146, 9)), ballToCoord(new Point('I', 5)))
                    .L(ballToCoord(new Point('I', 26, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 77: 
                    path.
                    Q(ballToCoord(new Point('I', 2, 9)), localFigure.getRedPosition());
                    break;
                case 78: 
                    path.
                    Q(ballToCoord(new Point('L', 51)), ballToCoord(new Point('N', 4)))
                    .Q(ballToCoord(new Point('O', 51)), ballToCoord(new Point('P', 21)))
                    .L(ballToCoord(new Point('P', 3)))
                    .L(localFigure.getRedPosition());
                    break;
                case 79: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('C', 33)), ballToCoord(new Point('A', 16)))
                    .L(ballToCoord(new Point('A', 3)))
                    .L(localFigure.getRedPosition());
                    break;
                case 80: 
                    path.
                    L(ballToCoord(figure.jaune))
                    .L(ballToCoord(new Point('P', 1, 9)))
                    .L(ballToCoord(new Point('P', 11)))
                    .Q(ballToCoord(new Point('P', 135, 9)), ballToCoord(new Point('A', 116)))
                    .L(ballToCoord(new Point('A', 3)))
                    .L(ballToCoord(figure.rouge));
                    break;
                case 81: 
                    path.
                    Q(ballToCoord(new Point('K', 135)), ballToCoord(new Point('I', 146, 7)))
                    .Q(ballToCoord(new Point('K', 135)), ballToCoord(new Point('E', 3, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 82: 
                    path
                        .Q(ballToCoord(new Point('M', 113)), ballToCoord(new Point('L', 2, 9)))
                        .Q(ballToCoord(new Point('K', 56)), localFigure.getRedPosition());
                    break;
                case 83: 
                    path.
                    L(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('N', 1, 9)))
                    .Q(ballToCoord(new Point('M', 113)), ballToCoord(new Point('A', 4, 9)))
                    .L(ballToCoord(new Point('A', 16, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 84: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('J', 131)), ballToCoord(new Point('K', 2)))
                    .Q(ballToCoord(new Point('M', 156, 9)), ballToCoord(new Point('A', 46)))
                    .L(ballToCoord(new Point('A', 2)))
                    .L(localFigure.getRedPosition());
                    break;
                case 85: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('L', 133)), ballToCoord(new Point('I', 36)))
                    .L(ballToCoord(new Point('I', 3)))
                    .L(ballToCoord(new Point('A', 46)))
                    .L(localFigure.getRedPosition());
                    break;
                case 86: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('B', 13, 9)), ballToCoord(new Point('C', 3, 9)))
                    .Q(ballToCoord(new Point('F', 16, 9)), ballToCoord(new Point('H', 6, 9)))
                    .L(ballToCoord(new Point('H', 1, 7)))
                    .L(ballToCoord(new Point('H', 3)))
                    .L(localFigure.getRedPosition());
                    break;
                case 87: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('O', 13, 9)), ballToCoord(new Point('N', 6, 9)))
                    .Q(ballToCoord(new Point('K', 16, 9)), ballToCoord(new Point('J', 6, 9)))
                    .L(ballToCoord(new Point('I', 6, 7)))
                    .L(localFigure.getRedPosition());
                    break;
                case 88: 
                    path.
                    Q(ballToCoord(new Point('O', 136)), ballToCoord(new Point('P', 2)))
                    .L(ballToCoord(new Point('P', 11)))
                    .Q(ballToCoord(new Point('B', 156, 8)), ballToCoord(new Point('A', 1)))
                    .Q(ballToCoord(new Point('N', 131, 8)), ballToCoord(new Point('O', 2, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 89: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('B', 13, 9)), ballToCoord(new Point('B', 4)))
                    .Q(ballToCoord(new Point('A', 42, 9)), ballToCoord(new Point('A', 26)))
                    .L(ballToCoord(new Point('A', 3, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 90: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('H', 43)), ballToCoord(new Point('H', 21)))
                    .L(ballToCoord(new Point('H', 2)))
                    .Q(ballToCoord(new Point('H', 156, 9)), ballToCoord(new Point('I', 4)))
                    .L(localFigure.getRedPosition());
                    break;
                case 91: 
                    path.
                    Q(ballToCoord(new Point('B', 33)), ballToCoord(new Point('B', 1, 9)))
                    .Q(ballToCoord(new Point('C', 102, 9)), ballToCoord(new Point('A', 4, 9)))
                    .L(ballToCoord(new Point('A', 16)))
                    .L(ballToCoord(new Point('M', 1, 9)))
                    .L(findMiddle(ballToCoord(figure.jaune), ballToCoord(figure.rouge)));
                    break;
                case 92: 
                    var curveInflexionPoint = ballToCoord(new Point('B', 4, 9));
                    curveInflexionPoint.x *= -25; //WARN force it because it's outside the billiard
                    path	
                        .Q(curveInflexionPoint, ballToCoord(new Point('I', 106)))	
                        .L(ballToCoord(new Point('I', 1)))	
                        .L(localFigure.getRedPosition());
                    break;
                case 93: 
                    path.
                    L(ballToCoord(figure.jaune))
                    .Q(ballToCoord(new Point('O', 21)), ballToCoord(new Point('P', 121)))
                    .L(ballToCoord(new Point('B', 1)))
                    .L(ballToCoord(figure.rouge));
                    break;
                case 94: 
                    path.
                    L(localFigure.getYellowPosition())
                    .Q(ballToCoord(new Point('O', 106)), ballToCoord(new Point('P', 21)))
                    .L(ballToCoord(new Point('P', 2, 9)))
                    .L(localFigure.getRedPosition());
                lastGroup.path(
                    )
                    .m(localFigure.getYellowPosition())
                    .L(ballToCoord(new Point('I', 2, 9)))
                    .L(ballToCoord(new Point('I', 36)))
                    .L(ballToCoord(new Point('D', 5)))
                    .L(ballToCoord(new Point('P', 141)))
                    .L(ballToCoord(new Point('O', 2, 9)))
                    .L(localFigure.getRedPosition())
                    .fill('none')
                    .stroke({
                        color: 'yellow',
                        width: figureLineWidth
                    });
                var zoneRappelSize = (billardWidth / 24) * 9 * 2;
                lastGroup.circle(zoneRappelSize)
                    .fill('rgba(255,255,0,0.3)')
                    .move(-zoneRappelSize / 2, -zoneRappelSize / 2) ;
                    break;
                case 95: 
                    path.
                    Q(ballToCoord(new Point('J', 54)), ballToCoord(new Point('F', 2, 9)))
                    .L(ballToCoord(new Point('P', 111)))
                    .L(localFigure.getRedPosition());
                    break;
                case 96: 
                    path.
                    Q(ballToCoord(new Point('P', 152, 9)), ballToCoord(new Point('P', 11)))
                    .L(ballToCoord(new Point('P', 11)))
                    .L(ballToCoord(new Point('P', 1, 9)))
                    .L(ballToCoord(new Point('P', 31)))
                    .L(localFigure.getRedPosition());
                    break;
                case 97: 
                    path.
                    Q(ballToCoord(new Point('J', 151)), ballToCoord(new Point('I', 4)))
                    .L(ballToCoord(new Point('I', 26)))
                    .L(localFigure.getRedPosition());
                    break;
                case 98: 
                    path.
                    Q(ballToCoord(new Point('K', 156)), ballToCoord(new Point('K', 3)))
                    .Q(ballToCoord(new Point('F', 142, 9)), ballToCoord(new Point('E', 1)))
                    .Q(ballToCoord(new Point('E', 152, 9)), ballToCoord(new Point('O', 6, 9)))
                    .L(ballToCoord(new Point('P', 151, 7)))
                    .L(ballToCoord(new Point('C', 6, 9)))
                    .L(localFigure.getRedPosition());
                    break;
                case 99: 
                    path.
                    Q(ballToCoord(new Point('P', 42)), ballToCoord(new Point('P', 101)))
                    .Q(ballToCoord(new Point('P', 122, 9)), ballToCoord(new Point('P', 151)))
                    .Q(ballToCoord(new Point('A', 53)), ballToCoord(new Point('P', 41)))
                    .L(localFigure.getRedPosition());
                    break;
                case 100: 
                    path.
                    Q(ballToCoord(new Point('P', 151)), ballToCoord(new Point('C', 3, 9)))
                    .L(ballToCoord(new Point('I', 56)))
                    .L(ballToCoord(new Point('J', 6)))
                    .L(localFigure.getRedPosition());
                    break;
                default:
                    console.log(`figure ${figure.name} not found`);
            }
        }
        path.fill('none').stroke({ color: '#fff', width: figureLineWidth });

        if (animate) {
            var movingBall = lastGroup.circle((billardWidth / 48) * 2).fill('white')
            length = path.length();

            movingBall.animate( length,'expoOut')
                //.ease('elastic')
                .during(function (pos, morph) {
                    var p = path.pointAt(pos * length)
                    movingBall.center(p.x, p.y)
                }).loop(true,null, 1000)
        }

    }

    //console.log(lastGroup);
    return lastGroup;

}



var drawFigure = function (groupSVG, figure, fillSector, drawOnlyDots) {
    //console.log(figure.name);

    var coordWhite, coordYellow;
    if (!figure.blanche.isOnLine) {
        coordWhite = drawBall(groupSVG, figure.blanche, 'blue', 'white', fillSector, false, false, drawOnlyDots, drawOnlyDots?figure.name:null, figure.blanche.legendPosition);
    }
    else {
        coordYellow = drawBall(groupSVG, figure.jaune, 'green', '#ffc800', fillSector, false, false, drawOnlyDots, drawOnlyDots?figure.name:null, figure.jaune.legendPosition);
        //find ball coordinate    
        //Calculate distance 
        var lineEndCoords = drawBall(groupSVG, figure.lines[0], 'blue', 'white', false, true);

        var closestPoint = findClosestPointUntil(coordYellow, lineEndCoords, (billardWidth / 24 + 2));

        if (figure.lines.length > 1) {
            var lineEndCoords2 = drawBall(groupSVG, figure.lines[1], 'blue', 'white', false, true);
            var closestPoint2 = findClosestPointUntil(coordYellow, lineEndCoords2, (billardWidth / 24 + 2));

            closestPoint = findMiddle(closestPoint, closestPoint2);
        }


        coordWhite=closestPoint;

        if (!drawOnlyDots) {
            

            groupSVG.circle((billardWidth / 48) * 2).fill('#fff').move(closestPoint.x - (billardWidth / 48), closestPoint.y - (billardWidth / 48))


        }
    }

    if (!figure.jaune.isOnLine) {
        coordYellow = drawBall(groupSVG, figure.jaune, 'green', '#ffc800', fillSector, false, drawOnlyDots, drawOnlyDots, drawOnlyDots?figure.name:null, figure.jaune.legendPosition);
    } else {

        //find ball coordinate    
        //Calculate distance 
        var lineEndCoords = drawBall(groupSVG, figure.lines[0], 'blue', 'white', false, true);

        var closestPoint = findClosestPointUntil(coordWhite, lineEndCoords, (billardWidth / 24 + 2));
        if (figure.lines.length > 1) {
            var lineEndCoords2 = drawBall(groupSVG, figure.lines[1], 'blue', 'white', false, true);
            var closestPoint2 = findClosestPointUntil(coordWhite, lineEndCoords2, (billardWidth / 24 + 2));

            closestPoint = findMiddle(closestPoint, closestPoint2);
        }

        coordYellow=closestPoint;
        if (!drawOnlyDots) {

            groupSVG.circle((billardWidth / 48) * 2).fill('#ffc800').move(closestPoint.x - (billardWidth / 48), closestPoint.y - (billardWidth / 48))

        }
    }

    coordRed=drawBall(groupSVG, figure.rouge, 'purple', 'red', fillSector, false, drawOnlyDots, drawOnlyDots, drawOnlyDots?figure.name:null, figure.rouge.legendPosition);
    if (figure.limit) {
        drawBall(groupSVG, figure.limit, 'green', '#ffc800', fillSector, false, true, true, drawOnlyDots?figure.name:null, figure.limit.legendPosition);
    }
    if (figure.lines) {
        figure.lines.forEach(function (item) {
            var lineEndCoords = drawBall(groupSVG, item, 'blue', 'white', false, true);

            var realLineEnd;
            var line;
            if (figure.jaune.isOnLine) {
                realLineEnd = findClosestPointUntil(coordWhite, lineEndCoords, billardWidth / 8) || lineEndCoords;

                line = groupSVG.line(coordWhite.x, coordWhite.y, realLineEnd.x, realLineEnd.y);

            } else {
                realLineEnd = findClosestPointUntil(coordYellow, lineEndCoords, billardWidth / 8) || lineEndCoords;

                line = groupSVG.line(coordYellow.x, coordYellow.y, realLineEnd.x, realLineEnd.y);

            }

            line.stroke({ color: '#000', width: 1, 'dasharray': '3, 3' });

        });
    }

    //console.log({figure:figure.name,blanche:coordWhite,jaune:coordYellow,rouge:coordRed})
    //console.log(figure.name);


    //console.log(`${figure.name};${coordWhite.x};${coordWhite.y};${coordYellow.x};${coordYellow.y};${coordRed.x};${coordRed.y}`)

};

var ballToCoord = function (ball) {
    var tmp = ballToCoordInArray(ball);
    return { x: tmp[0], y: tmp[1] };
}
var ballToCoordInArray = function (ball) {
    var tmpSecteur = (ball.secteur.toLowerCase().charCodeAt(0) - 'a'.charCodeAt(0));

    var localSecteur = tmpSecteur % 8;
    var isRight = Math.floor(tmpSecteur / 8);


    var coordX, coordY;
    if (!isRight) {


        var y = 6 - ball.point % 10;
        var x = Math.floor(ball.point / 10);


        coordY = ((billardWidth * 2) - ((y + 6 * localSecteur) * ((billardWidth * 2) / 48))) - (billardWidth * 2) / 96;
        if (ball.satellite == 8 || ball.satellite == 9) {
            coordY += (billardWidth * 2) / 96;
        }
        if (x > 5) {
            x -= 4;
        }
        coordX = (x * billardWidth / 24) + billardWidth / 48;
        if (ball.satellite == 7 || ball.satellite == 8) {
            coordX += billardWidth / 48;
        }




    } else {

        var y = 6 - ball.point % 10;
        var x = Math.floor(ball.point / 10);

        var coordY = ((y + 6 * localSecteur) * ((billardWidth * 2) / 48)) + (billardWidth * 2) / 96;
        if (ball.satellite == 8 || ball.satellite == 9) {
            coordY -= (billardWidth * 2) / 96;
        }
        if (x > 5) {
            x -= 4;
        }
        var coordX = billardWidth - (x * billardWidth / 24) - billardWidth / 48;
        if (ball.satellite == 7 || ball.satellite == 8) {
            coordX -= billardWidth / 48;
        }

    }

    return [coordX, coordY];
};
var drawBall = function (groupSVG, ball, color, ballColor, fillSector, calculateOnly, isLimit, isDot, legend, legendPosition) {

    
    var tmpSecteur = (ball.secteur.toLowerCase().charCodeAt(0) - 'a'.charCodeAt(0));

    var localSecteur = tmpSecteur % 8;
    var isRight = Math.floor(tmpSecteur / 8);


    var coordX, coordY;
    if (!isRight) {

        if (fillSector) {
            ctx.fillStyle = "rgba(80,80,80,0.5)";
            ctx.fillRect(billardWidth * isRight / 2, (billardWidth * 2) - ((localSecteur + 1) * (billardWidth * 2) / 8), billardWidth / 2, (billardWidth * 2) / 8);
        }
        var y = 6 - ball.point % 10;
        var x = Math.floor(ball.point / 10);


        coordY = ((billardWidth * 2) - ((y + 6 * localSecteur) * ((billardWidth * 2) / 48))) - (billardWidth * 2) / 96;
        if (ball.satellite == 8 || ball.satellite == 9) {
            coordY += (billardWidth * 2) / 96;
        }
        if (x > 5) {
            x -= 4;
        }
        coordX = (x * billardWidth / 24) + billardWidth / 48;
        if (ball.satellite == 7 || ball.satellite == 8) {
            coordX += billardWidth / 48;
        }




    } else {
        if (fillSector) {
            ctx.fillStyle = "rgba(80,80,80,0.5)";
            ctx.fillRect(billardWidth * isRight / 2, ((localSecteur) * (billardWidth * 2) / 8), billardWidth / 2, (billardWidth * 2) / 8);
        }
        var y = 6 - ball.point % 10;
        var x = Math.floor(ball.point / 10);

        var coordY = ((y + 6 * localSecteur) * ((billardWidth * 2) / 48)) + (billardWidth * 2) / 96;
        if (ball.satellite == 8 || ball.satellite == 9) {
            coordY -= (billardWidth * 2) / 96;
        }
        if (x > 5) {
            x -= 4;
        }
        var coordX = billardWidth - (x * billardWidth / 24) - billardWidth / 48;
        if (ball.satellite == 7 || ball.satellite == 8) {
            coordX -= billardWidth / 48;
        }

    }


    if (drawPositionLines) {
        ctx.beginPath();
        ctx.moveTo(0, coordY);
        ctx.lineTo(billardWidth, coordY);
        ctx.strokeStyle = color;
        ctx.stroke();


        ctx.beginPath();
        ctx.moveTo(coordX, 0);
        ctx.lineTo(coordX, (billardWidth * 2));
        ctx.strokeStyle = color;
        ctx.stroke();
    }

    if (!calculateOnly) {
        if (isLimit) {
            var limit = groupSVG.circle(limitSize * 2).fill('black').move(coordX - limitSize, coordY - limitSize)
        } else if (isDot) {
            var dot = groupSVG.circle(limitSize * 2).fill('black').move(coordX - limitSize, coordY - limitSize)
        }
        else {
            var ball = groupSVG.circle((billardWidth / 48) * 2).fill(ballColor).move(coordX - (billardWidth / 48), coordY - (billardWidth / 48))
        }

        if (legend && legendPosition) {
            
            var txt = groupSVG.text().font({size:billardWidth / 65, family:' Consolas'}).move(coordX, coordY);
            switch (legendPosition) {
                case LEGEND_POSITION.N:
                    txt.text(legend).transform({rotate:(isRight ? (-1) : 1) * 90,originX : coordX, originY:coordY,translateX:(isRight ? (-5) : 5),translateY:(isRight ? (5) : -5)})
                    break;
                case LEGEND_POSITION.NE:
                    txt.text(legend).transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (-5) : 5), translateY: (isRight? -4: 4)});
                    break;
                case LEGEND_POSITION.NEE:
                    txt.text(',' + legend).transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (-5) : 5), translateY: (isRight? -18/*FIXME*/ : 15)});
                    break;
                case LEGEND_POSITION.NW:
                    txt.text(legend).transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (-5) : 5), translateY: (isRight? -3: -14)});
                    break;
                case LEGEND_POSITION.S:
                    txt.text(legend).transform({rotate:(isRight ? (-1) : 1) * 90,originX : coordX, originY:coordY, translateX: (isRight ? (12) : -12), translateY:(isRight ? (5) : -5)});
                    break;
                case LEGEND_POSITION.SS:
                    txt.text(legend).transform({rotate:(isRight ? (-1) : 1) * 90,originX : coordX, originY:coordY,  translateX: (isRight ? (24) : -24), translateY:(isRight ? (5) : -5)});
                    break;
                case LEGEND_POSITION.SE:
                    txt.text(legend).transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (12) : -12), translateY:(isRight ? (-4) : 4)});
                    break;
                case LEGEND_POSITION.SEE:
                    txt.text(',' + legend).transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (12) : -12), translateY:(isRight ? (-2) : 13)});
                    break;
                case LEGEND_POSITION.SW:
                    txt.text(legend).transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (12) : -12), translateY:(isRight ? (12) : -13)});
                    break;
                case LEGEND_POSITION.SWW:
                    txt.text(legend + ',').transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (12) : -12), translateY:(isRight ? (12) : -24)});
                    break;
                case LEGEND_POSITION.W:
                    txt.text(legend).transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (3) : -3), translateY: (isRight ? (16) : -16)});
                    break;
                case LEGEND_POSITION.WW:
                    txt.text(legend+',').transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (3) : -3), translateY: (isRight ? (32) : -32)});
                    break;
                case LEGEND_POSITION.WWW:
                    txt.text(legend+',').transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (3) : -3), translateY: (isRight ? (42) : -42)});
                    break;
                case LEGEND_POSITION.E:
                    txt.text(legend).transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (3) : -3), translateY: (isRight ? (-4) : 4)});
                    break;
                case LEGEND_POSITION.EE:
                    txt.text(',' + legend).transform({rotate:(isRight ? (-1) : 1) * 90, originX : coordX, originY:coordY, translateX: (isRight ? (3) : -3), translateY: (isRight ? (-16) : 16)});
                    break;
                default:
                    break;
            }
        }
    }
    return { x: coordX, y: coordY };
}


var drawAllDots = function () {
    if (lastGroup)
        lastGroup.remove();

    lastGroup = cloth.group();


    // var canvas = document.getElementById('canvas');
    // if (canvas.getContext) {
    //     var ctx = canvas.getContext('2d');

    //     ctx.clearRect(0, 0, canvas.width, canvas.height);
    //     ctx.fillStyle = '#0f7933';
    //     ctx.fillRect(0, 0, billardWidth, (billardWidth * 2));
    //     ctx.strokeStyle = '#000';
    //     ctx.strokeRect(0, 0, billardWidth, (billardWidth * 2));
    //     if (drawGrid) {
    //         for (var i = 1; i < 25; i++) {
    //             if ((i) % 12 == 0 || drawInternalGrid) {
    //                 ctx.beginPath();
    //                 ctx.moveTo(i * (billardWidth / 24), 0);
    //                 ctx.lineTo(i * (billardWidth / 24), (billardWidth * 2));

    //                 if ((i) % 12 == 0) {
    //                     ctx.strokeStyle = '#ff0000';
    //                     ctx.stroke();
    //                 } else {
    //                     ctx.strokeStyle = '#000';
    //                     ctx.stroke();
    //                 }
    //             }
    //         }
    //         for (var i = 1; i < 49; i++) {
    //             if ((i) % 6 == 0 || drawInternalGrid) {
    //                 ctx.beginPath();
    //                 ctx.moveTo(0, i * ((billardWidth * 2) / 48));
    //                 ctx.lineTo(billardWidth, i * ((billardWidth * 2) / 48));

    //                 if ((i) % 6 == 0) {
    //                     ctx.strokeStyle = '#ff0000';
    //                     ctx.stroke();
    //                 } else {
    //                     ctx.strokeStyle = '#000';
    //                     ctx.stroke();
    //                 }
    //             }
    //         }
    //     }

        Object.keys(figures)/*.filter((x)=> {return x=='_8' || x=='_30'})*/.forEach(function (item) { drawFigure(lastGroup, figures[item], false, true) })
    //}
}

var findMiddle = function (from, to) {
    return { x: (from.x + to.x) / 2, y: (from.y + to.y) / 2 };
}
var findClosestPointUntil = function (from, to, minLength) {
    var quantity = 128;
    var ydiff = to.y - from.y, xdiff = to.x - from.x;
    var slope = (to.y - from.y) / (to.x - from.x);
    var x, y;

    var closestPoint;
    for (var i = 0; i < quantity; i++) {
        y = slope == 0 ? 0 : ydiff * (i / quantity);
        x = slope == 0 ? xdiff * (i / quantity) : y / slope;
        _x = Math.round(x) + from.x;
        _y = Math.round(y) + from.y;
        distance = Math.sqrt(Math.pow(_x - from.x, 2) + Math.pow(_y - from.y, 2));

        if (distance > minLength) {
            closestPoint = { x: _x, y: _y };
            return closestPoint;
            break;
        }
    }
}
var drawFigureNumber = function(groupSVG, figure,){
    var txt = groupSVG.text(figure.name).font({anchor: 'middle',size:billardWidth /2, family:' Consolas',fill: '#808080',opacity: 0.6}).center(billardWidth/2, billardWidth);
}

var displayDetails = function(figure){

    document.querySelector('#whiteBall .position').innerHTML = '';
    document.querySelector('#yellowBall .position').innerHTML = '';
    document.querySelector('#redBall .position').innerHTML = '';
    document.querySelector('#limit .position').innerHTML = '';
    if(!figure)
        return;
    if(!figure.blanche.isOnLine){
        document.querySelector('#whiteBall .position').innerHTML = `Secteur ${figure.blanche.secteur}, point ${figure.blanche.point}`;
        if(figure.blanche.satellite)
            document.querySelector('#whiteBall .position').innerHTML+=` satellite ${figure.blanche.satellite}`;
    } else {
        document.querySelector('#whiteBall .position').innerHTML = 'Sur la ligne'
    }
    if(!figure.jaune.isOnLine){
        document.querySelector('#yellowBall .position').innerHTML = `Secteur ${figure.jaune.secteur}, point ${figure.jaune.point}`;
        if(figure.jaune.satellite)
            document.querySelector('#yellowBall .position').innerHTML+=` satellite ${figure.jaune.satellite}`;
    } else {
        document.querySelector('#yellowBall .position').innerHTML = 'Sur la ligne'
    }
    document.querySelector('#redBall .position').innerHTML = `Secteur ${figure.rouge.secteur}, point ${figure.rouge.point}`
    if(figure.rouge.satellite)
        document.querySelector('#redBall .position').innerHTML+=` satellite ${figure.rouge.satellite}`;
    if(figure.limit){
        document.querySelector('#limit').style.display = 'initial';
        document.querySelector('#limit .position').innerHTML = `Secteur ${figure.limit.secteur}, point ${figure.limit.point}`
        if(figure.limit.satellite)
            document.querySelector('#limit .position').innerHTML+=` satellite ${figure.limit.satellite}`;
    } else {
        document.querySelector('#limit').style.display='none';
    }

}
var drawSVG;
var cloth;
var lastGroup;
document.addEventListener('DOMContentLoaded', function () {

    var cushionWidth = billardWidth/24;
    woodWidth = cushionWidth*1.5;
    var fullBillardWidth = billardWidth+(cushionWidth + woodWidth)*2;
    var fullBillardHeigth = billardWidth*2+(cushionWidth + woodWidth)*2;

    drawSVG = SVG().addTo('#drawing').size(fullBillardWidth,fullBillardHeigth)
    wood = drawSVG.nested().size(fullBillardWidth,fullBillardHeigth).attr({ x: 0, y: 0 });

    cushions = drawSVG.nested().size(billardWidth+cushionWidth*2,(billardWidth * 2)+cushionWidth*2).attr({ x: woodWidth, y: woodWidth });
    cloth = drawSVG.nested().size(billardWidth, billardWidth * 2).attr({ x: (cushionWidth + woodWidth), y: (cushionWidth + woodWidth) });
    

    var rectWood = wood.rect(fullBillardWidth,fullBillardHeigth).attr({
        fill: '#a82b2b'
        
    });
    rectWood.radius(25);
    var rectCushion = cushions.rect(billardWidth+cushionWidth*2,(billardWidth * 2)+cushionWidth*2).attr({
        fill: clothColor
        , stroke: '#888'
        , 'stroke-width': 2
    });
    var rect = cloth.rect(billardWidth, billardWidth * 2).attr({
        fill: clothColor
        , stroke: '#888'
        , 'stroke-width': 8
    });

    var diamondSize = limitSize*2;
    for(i=0;i<9;i++){
        // wood.polygon(`${diamondSize},0 ${diamondSize*4},${diamondSize} ${diamondSize},${diamondSize*2} -${diamondSize*2},${diamondSize} ${diamondSize},0`).fill('white').move((woodWidth/2)-diamondSize*2,((billardWidth*2/8)*i)+(woodWidth+cushionWidth)-diamondSize).transform({
        //     rotate: 0,
        // });
        wood.rect(diamondSize,diamondSize).fill('white').move((woodWidth/2)-limitSize,((billardWidth*2/8)*i)+(woodWidth+cushionWidth)-limitSize).transform({
            rotate: 45,
        });
        wood.rect(diamondSize,diamondSize).fill('white').move(fullBillardWidth - ((woodWidth/2)+limitSize),((billardWidth*2/8)*i)+(woodWidth+cushionWidth)-limitSize).transform({
            rotate: 45,
        });    
    }

    for(i=0;i<5;i++){
        wood.rect(diamondSize,diamondSize).fill('white').move(((billardWidth*2/8)*i)+(woodWidth+cushionWidth)-limitSize,(woodWidth/2)-limitSize).transform({
            rotate: 45,
            skewX:2
        });
        wood.rect(diamondSize,diamondSize).fill('white').move(((billardWidth*2/8)*i)+(woodWidth+cushionWidth)-limitSize,fullBillardHeigth - (woodWidth/2)-limitSize/2).transform({
            rotate: 45,
        });
    }


    //    drawAllDots();
    if (drawGrid) {
        for (var i = 1; i < 25; i++) {
            if ((i) % 12 == 0 || drawInternalGrid) {
                var line = cloth.line(i * (billardWidth / 24), 0, i * (billardWidth / 24), (billardWidth * 2));

                if ((i) % 12 == 0) {
                    line.stroke({ color: '#ff0000', width: 1 })
                } else {
                    line.stroke({ color: '#000', width: 1 })
                }
            }
        }
        for (var i = 1; i < 49; i++) {
            if ((i) % 6 == 0 || drawInternalGrid) {
                var line = cloth.line(0, i * ((billardWidth * 2) / 48), billardWidth, i * ((billardWidth * 2) / 48))
                if ((i) % 6 == 0) {
                    line.stroke({ color: '#ff0000', width: 1 })
                } else {
                    line.stroke({ color: '#000', width: 1 })
                }
            }
        }
    }
    for(var i=1;i<=100;i++){
        option = new Option(`_${i}`,`_${i}`);
        document.querySelector('#figureSelect').appendChild(option);
    }
    // for (let [key, value] of Object.entries(figures)) {
    //     option = new Option(key,key);
    //     document.querySelector('#figureSelect').appendChild(option);
    // }
    document.querySelector('#figureSelect').addEventListener('change',(evt)=>{
        draw(figures[evt.target.value]);
        
        // Construct URLSearchParams object instance from current URL querystring.
        var queryParams = new URLSearchParams(window.location.search);
        
        // Set new or modify existing parameter value. 
        queryParams.set("figure", evt.target.value.substring(1));
        
        // Replace current querystring with the new one.
        history.replaceState(null, null, "?"+queryParams.toString());
    })

    document.querySelector('#isLeftHanded').addEventListener('change',(evt)=>{
        isLeftHanded=evt.target.checked;
        if(isLeftHanded)
            figures = figuresLeftHanded;
        else 
            figures = figuresRightHanded;

        document.querySelector('#figureSelect').dispatchEvent(new Event('change'));

        // Construct URLSearchParams object instance from current URL querystring.
        var queryParams = new URLSearchParams(window.location.search);
        
        // Set new or modify existing parameter value. 
        queryParams.set("isLeftHanded", isLeftHanded);
        
        // Replace current querystring with the new one.
        history.replaceState(null, null, "?"+queryParams.toString());
    });

    if(landscapeMode){
        cloth.transform({
            rotate: 90,
            translate: [-billardWidth, -billardWidth]
        });
    }

    drawAllDots();
    try{
        document.querySelector('#figureSelect').value='_'+new URLSearchParams(window.location.search).get('figure');
        document.querySelector('#figureSelect').dispatchEvent(new Event('change'));
    } catch(exception){}

    try{
        document.querySelector('#isLeftHanded').checked=new URLSearchParams(window.location.search).get('isLeftHanded') == "true";
        document.querySelector('#isLeftHanded').dispatchEvent(new Event('change'));
    } catch(exception){}

});

var downloadAllFigure=function(){
    for(var i = 1; i<=100;i++){
        draw(figures['_'+i])
        console.log(i);
        var svg = document.querySelector('#drawing > svg').outerHTML;
          var element = document.createElement('a');
          element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(svg));
          element.setAttribute('download', i+'.svg');
      
          element.style.display = 'none';
          document.body.appendChild(element);
      
          element.click();
      
          document.body.removeChild(element);
      }
}
