const LEGEND_POSITION = {
    N: 'NORTH',
    S: 'SOUTH',
    E: 'EAST',
    EE: 'EAST_EAST',
    W: 'WEST',
    WW: 'WEST_WEST',
    WWW: 'WEST_WEST_WEST',
    NE: 'NORTH_EAST',
    NEE: 'NORTH_EAST_EAST',
    NW: 'NORTH_WEST',
    SW: 'SOUTH_WEST',
    SWW: 'SOUTH_WEST_WEST',
    SE: 'SOUTH_EAST',
    SEE: 'SOUTH_EAST_EAST',
    SS: 'SOUTH_SOUTH'
}
const Point = class {
    constructor(secteur, point, satellite, legendPosition, isOnLine) {
        this.secteur = secteur;
        this.point = point;
        this.satellite = satellite;
        this.legendPosition = legendPosition;
        this.isOnLine = isOnLine;
    }
};

const OnLinePoint = class extends Point {
    constructor() {
        super(null, null, null, null, true);
    }
}

const Figure = class {
    constructor(blanche, jaune, rouge, lines, limit, name) {
        this.blanche = blanche;
        this.jaune = jaune;
        this.rouge = rouge;
        this.lines = lines;
        this.limit = limit;
        this.name = name;
    }

    getWhitePosition() {
        if (!this.blanche.isOnLine) {
            return ballToCoord(this.blanche);
        } else {
            var coordYellow = ballToCoord(this.jaune);
            var lineEndCoords = ballToCoord(this.lines[0]);
            return findClosestPointUntil(coordYellow, lineEndCoords, (billardWidth / 24 + 2));

        }
    }
    getYellowPosition() {
        if (!this.jaune.isOnLine) {
            return ballToCoord(this.jaune);
        } else {
            var coordWhite = ballToCoord(this.blanche);
            var lineEndCoords = ballToCoord(this.lines[0]);
            return findClosestPointUntil(coordWhite, lineEndCoords, (billardWidth / 24 + 2));

        }
    }
    getRedPosition() {
        return ballToCoord(this.rouge);
    }

    /*var coordWhite, coordYellow;
    if (!figure.blanche.isOnLine) {
        coordWhite = drawBall(groupSVG, figure.blanche, 'blue', 'white', fillSector, false, false, drawOnlyDots, figure.name, figure.blanche.legendPosition);
    }
    else {
        var coordYellow = drawBall(groupSVG, figure.jaune, 'green', '#ffc800', fillSector, false, false, drawOnlyDots, figure.name, figure.jaune.legendPosition);
        //find ball coordinate    
        //Calculate distance 
        var lineEndCoords = drawBall(groupSVG, figure.lines[0], 'blue', 'white', false, true);

        var closestPoint = findClosestPointUntil(coordYellow, lineEndCoords, (billardWidth / 24 + 2));

        if (!drawOnlyDots) {

            groupSVG.circle((billardWidth / 48) * 2).fill('#fff').move(closestPoint.x - (billardWidth / 48), closestPoint.y - (billardWidth / 48))


        }
    }*/
}
var figuresRightHanded = {
    _54: { blanche: new Point('O', 116, 9, LEGEND_POSITION.E), jaune: new Point('L', 142, 7, LEGEND_POSITION.E), rouge: new Point('H', 13, undefined, LEGEND_POSITION.NEE), name: 54 }
    , _61: { blanche: new Point('P', 23, 7, LEGEND_POSITION.N), jaune: new Point('P', 36, 8, LEGEND_POSITION.E), rouge: new Point('O', 6, 8, LEGEND_POSITION.E), name: 61 }
    , _95: { blanche: new Point('I', 55, 8, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 16, 9), lines: [new Point('I', 15, 8)], name: 95 }
    , _78: { blanche: new OnLinePoint(), jaune: new Point('I', 113, 9, LEGEND_POSITION.SE), rouge: new Point('H', 15, undefined, LEGEND_POSITION.NE), lines: [new Point('I', 153, 7)], name: 78 }
    , _9: { blanche: new Point('P', 16, 7, LEGEND_POSITION.W), jaune: new Point('P', 52, 7, LEGEND_POSITION.E), rouge: new Point('A', 114, undefined, LEGEND_POSITION.E), name: 9 }
    , _71: { blanche: new Point('A', 33, 9, LEGEND_POSITION.SW), jaune: new OnLinePoint(), rouge: new Point('A', 25, undefined), lines: [new Point('A', 3, 9)], name: 71 }
    , _93: { blanche: new Point('M', 153, 7, LEGEND_POSITION.E), jaune: new Point('M', 42, undefined, LEGEND_POSITION.EE), rouge: new Point('I', 14, undefined), limit: new Point('O', 53, 7, LEGEND_POSITION.E), name: 93 }
    , _53: { blanche: new Point('F', 35, undefined, LEGEND_POSITION.E), jaune: new Point('G', 34, 8, LEGEND_POSITION.E), rouge: new Point('H', 13, undefined, LEGEND_POSITION.NE), limit: new Point('G', 6, 8, LEGEND_POSITION.E), name: 53 }
    , _4: { blanche: new Point('O', 43, 7, LEGEND_POSITION.E), jaune: new Point('P', 53, 7, LEGEND_POSITION.E), rouge: new Point('P', 16, 9), name: 4 }
    , _6: { blanche: new Point('A', 4, undefined, LEGEND_POSITION.E), jaune: new Point('B', 6, 9, LEGEND_POSITION.WWW), rouge: new Point('B', 16, 9, LEGEND_POSITION.W), limit: new Point('N', 2, undefined, LEGEND_POSITION.E), name: 6 }
    , _72: { blanche: new Point('H', 23, undefined, LEGEND_POSITION.W), jaune: new OnLinePoint(), rouge: new Point('A', 34, undefined, LEGEND_POSITION.SE), lines: [new Point('I', 156, 7)], name: 72 }
    , _2: { blanche: new Point('K', 156, 8, LEGEND_POSITION.WW), jaune: new Point('I', 6, undefined, LEGEND_POSITION.W), rouge: new Point('P', 13, undefined, LEGEND_POSITION.SS), name: 2 }
    , _69: { blanche: new Point('O', 5, 8, LEGEND_POSITION.E), jaune: new Point('N', 14, 7, LEGEND_POSITION.E), rouge: new Point('P', 16, 8, LEGEND_POSITION.E), name: 69 }
    , _66: { blanche: new OnLinePoint(), jaune: new Point('N', 154, 8, LEGEND_POSITION.S), rouge: new Point('I', 14, undefined), lines: [new Point('O', 156, 8)], name: 66 }
    , _10: { blanche: new Point('J', 131, 7, LEGEND_POSITION.E), jaune: new Point('I', 25, undefined, LEGEND_POSITION.E), rouge: new Point('F', 16, undefined, LEGEND_POSITION.E), name: 10 }
    , _24: { blanche: new Point('O', 116, undefined, LEGEND_POSITION.W), jaune: new Point('G', 23, 9, LEGEND_POSITION.E), rouge: new Point('P', 13, 9, LEGEND_POSITION.S), name: 24 }
    , _67: { blanche: new Point('H', 51, 8, LEGEND_POSITION.E), jaune: new Point('G', 6, 9, LEGEND_POSITION.SE), rouge: new Point('G', 16, 8, LEGEND_POSITION.W), name: 67 }
    , _83: { blanche: new Point('O', 13, 9, LEGEND_POSITION.E), jaune: new Point('O', 26, 9, LEGEND_POSITION.E), rouge: new Point('A', 43, 8, LEGEND_POSITION.N), name: 83 }
    , _25: { blanche: new OnLinePoint(), jaune: new Point('I', 156, 7, LEGEND_POSITION.NE), rouge: new Point('I', 155, 7, LEGEND_POSITION.S), lines: [new Point('H', 23, 8), new Point('H', 122, 9)], name: 25 }
    , _33: { blanche: new Point('O', 56, 7, LEGEND_POSITION.W), jaune: new Point('N', 126, 9, LEGEND_POSITION.E), rouge: new Point('P', 13, undefined, LEGEND_POSITION.S), name: 33 }
    , _82: { blanche: new Point('P', 142, 7, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 14, undefined, LEGEND_POSITION.E), limit: new Point('M', 6, 9, LEGEND_POSITION.E), lines: [new Point('P', 52, undefined)], name: 82 }
    , _86: { blanche: new Point('O', 156, 8), jaune: new Point('A', 6, undefined, LEGEND_POSITION.N), rouge: new Point('G', 22, 9, LEGEND_POSITION.NE), name: 86 }
    , _58: { blanche: new Point('A', 23, 9, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('K', 13, 9, LEGEND_POSITION.E), lines: [new Point('A', 3, 9)], name: 58 }
    , _52: { blanche: new Point('F', 13, 9, LEGEND_POSITION.E), jaune: new Point('G', 26, 9, LEGEND_POSITION.E), rouge: new Point('B', 24, 9, LEGEND_POSITION.E), name: 52 }
    , _65: { blanche: new Point('I', 23, 9, LEGEND_POSITION.S), jaune: new OnLinePoint(), rouge: new Point('E', 16, 9, LEGEND_POSITION.E), lines: [new Point('I', 52, 9)], name: 65 }
    , _5: { blanche: new Point('O', 6, 8, LEGEND_POSITION.SE), jaune: new Point('N', 14, 7, LEGEND_POSITION.SE), rouge: new Point('P', 33, undefined, LEGEND_POSITION.E), name: 5 }
    , _99: { blanche: new OnLinePoint(), jaune: new Point('P', 23, 7, LEGEND_POSITION.S), rouge: new Point('P', 12, undefined, LEGEND_POSITION.S), lines: [new Point('P', 21, 7)], name: 99 }
    , _42: { blanche: new Point('L', 152, 8, LEGEND_POSITION.E), jaune: new Point('M', 16, 9, LEGEND_POSITION.E), rouge: new Point('G', 16, 9, LEGEND_POSITION.E), name: 42 }
    , _8: { blanche: new OnLinePoint(), jaune: new Point('H', 12, undefined, LEGEND_POSITION.S), rouge: new Point('A', 14, undefined), limit: new Point('N', 156, 8, LEGEND_POSITION.SW), lines: [new Point('H', 21, undefined)], name: 8 }
    , _80: { blanche: new Point('O', 34, 9, LEGEND_POSITION.E), jaune: new Point('P', 33, undefined, LEGEND_POSITION.N), rouge: new Point('A', 11, 8, LEGEND_POSITION.E), name: 80 }
    , _50: { blanche: new Point('O', 123, undefined, LEGEND_POSITION.E), jaune: new Point('O', 156, 8), rouge: new Point('H', 53, 7, LEGEND_POSITION.E), name: 50 }
    , _63: { blanche: new Point('O', 121, undefined, LEGEND_POSITION.E), jaune: new Point('O', 156, 8), rouge: new Point('C', 16, 9, LEGEND_POSITION.E), name: 63 }
    , _97: { blanche: new Point('I', 35, undefined, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('F', 12, 7, LEGEND_POSITION.E), limit: new Point('I', 51, undefined, LEGEND_POSITION.EE), lines: [new Point('I', 2, undefined)], name: 97 }
    , _41: { blanche: new Point('J', 151, 7, LEGEND_POSITION.E), jaune: new Point('F', 26, 9, LEGEND_POSITION.E), rouge: new Point('A', 34, undefined, LEGEND_POSITION.NE), name: 41 }
    , _92: { blanche: new Point('P', 23, 9, LEGEND_POSITION.S), jaune: new OnLinePoint(), rouge: new Point('A', 14, undefined), limit: new Point('C', 51, 9, LEGEND_POSITION.E), lines: [new Point('P', 26, undefined)], name: 92 }
    , _45: { blanche: new Point('P', 22, 8, LEGEND_POSITION.W), jaune: new Point('P', 104, 7, LEGEND_POSITION.E), rouge: new Point('B', 16, 7, LEGEND_POSITION.W), name: 45 }
    , _1: { blanche: new Point('F', 45, undefined, LEGEND_POSITION.E), jaune: new Point('G', 56, 8, LEGEND_POSITION.E), rouge: new Point('A', 14, undefined), name: 1 }
    , _68: { blanche: new Point('P', 6, 8, LEGEND_POSITION.E), jaune: new Point('H', 6, 9, LEGEND_POSITION.SE), rouge: new Point('H', 16, 8, LEGEND_POSITION.E), limit: new Point('B', 54, 8, LEGEND_POSITION.E), name: 68 }
    , _18: { blanche: new Point('C', 25, undefined, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('I', 14, undefined), limit: new Point('M', 6, 9, LEGEND_POSITION.SE), lines: [new Point('C', 46, 8)], name: 18 }
    , _94: { blanche: new Point('B', 45, 7, LEGEND_POSITION.E), jaune: new Point('C', 146, 9, LEGEND_POSITION.EE), rouge: new Point('H', 13, undefined, LEGEND_POSITION.SEE), name: 94 }
    , _3: { blanche: new Point('O', 122, undefined, LEGEND_POSITION.E), jaune: new Point('O', 13, 9, LEGEND_POSITION.SE), rouge: new Point('P', 32, undefined, LEGEND_POSITION.W), name: 3 }
    , _49: { blanche: new Point('O', 113, undefined, LEGEND_POSITION.E), jaune: new Point('J', 114, 9, LEGEND_POSITION.E), rouge: new Point('A', 14, undefined), name: 49 }
    , _46: { blanche: new Point('O', 146, 9, LEGEND_POSITION.W), jaune: new Point('I', 156, 7, LEGEND_POSITION.SE), rouge: new Point('P', 13, undefined, LEGEND_POSITION.W), name: 46 }
    , _81: { blanche: new Point('E', 36, 9, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 13, 9, LEGEND_POSITION.SS), limit: new Point('I', 156, 7, LEGEND_POSITION.SW), lines: [new Point('D', 143, undefined)], name: 81 }
    , _7: { blanche: new Point('A', 3, 9, LEGEND_POSITION.W), jaune: new Point('G', 6, 9, LEGEND_POSITION.SWW), rouge: new Point('A', 14, 9, LEGEND_POSITION.N), name: 7 }
    , _56: { blanche: new OnLinePoint(), jaune: new Point('C', 6, 9, LEGEND_POSITION.W), rouge: new Point('G', 22, 9, LEGEND_POSITION.NW), limit: new Point('C', 16, 9, LEGEND_POSITION.W), lines: [new Point('B', 2, 9), new Point('B', 12, 9)], name: 56 }
    , _31: { blanche: new Point('B', 13, 8, LEGEND_POSITION.E), jaune: new Point('C', 56, 8, LEGEND_POSITION.E), rouge: new Point('B', 52, 8, LEGEND_POSITION.E), name: 31 }
    , _84: { blanche: new Point('F', 4, 8, LEGEND_POSITION.N), jaune: new Point('G', 16, 9, LEGEND_POSITION.EE), rouge: new Point('I', 14, undefined), limit: new Point('L', 6, 9, LEGEND_POSITION.E), name: 84 }
    , _36: { blanche: new OnLinePoint(), jaune: new Point('D', 156, 8, LEGEND_POSITION.NW), rouge: new Point('A', 15, undefined, LEGEND_POSITION.E), lines: [new Point('D', 56, 8)], name: 36 }
    , _85: { blanche: new Point('A', 4, undefined, LEGEND_POSITION.EE), jaune: new Point('B', 6, 9, LEGEND_POSITION.W), rouge: new Point('B', 16, 9, LEGEND_POSITION.WW), name: 85 }
    , _17: { blanche: new Point('P', 34, 7, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('D', 13, 9, LEGEND_POSITION.E), lines: [new Point('P', 56, undefined)], name: 17 }
    , _22: { blanche: new Point('A', 44, undefined, LEGEND_POSITION.W), jaune: new Point('B', 46, 9, LEGEND_POSITION.SW), rouge: new Point('B', 26, 9, LEGEND_POSITION.E), name: 22 }
    , _44: { blanche: new Point('P', 123, 8, LEGEND_POSITION.E), jaune: new Point('A', 15, undefined, LEGEND_POSITION.SE), rouge: new Point('P', 11, 9, LEGEND_POSITION.W), name: 44 }
    , _70: { blanche: new Point('F', 134, 7, LEGEND_POSITION.E), jaune: new Point('K', 16, 9, LEGEND_POSITION.E), rouge: new Point('P', 32, 9, LEGEND_POSITION.N), limit: new Point('M', 32, 7, LEGEND_POSITION.E), name: 70 }
    , _60: { blanche: new Point('C', 126, 8, LEGEND_POSITION.E), jaune: new Point('A', 25, undefined, LEGEND_POSITION.NW), rouge: new Point('B', 45, undefined, LEGEND_POSITION.E), name: 60 }
    , _90: { blanche: new Point('F', 45, 7, LEGEND_POSITION.E), jaune: new Point('H', 123, undefined, LEGEND_POSITION.N), rouge: new Point('I', 55, 7, LEGEND_POSITION.W), name: 90 }
    , _48: { blanche: new OnLinePoint(), jaune: new Point('I', 113, 9, LEGEND_POSITION.W), rouge: new Point('P', 16, 9), lines: [new Point('I', 153, 7)], name: 48 }
    , _13: { blanche: new Point('M', 151, 8, LEGEND_POSITION.E), jaune: new Point('M', 42, undefined, LEGEND_POSITION.E), rouge: new Point('O', 16, 9, LEGEND_POSITION.E), limit: new Point('N', 55, 8, LEGEND_POSITION.E), name: 13 }
    , _16: { blanche: new Point('A', 44, undefined, LEGEND_POSITION.WW), jaune: new Point('B', 46, 9, LEGEND_POSITION.NW), rouge: new Point('P', 16, 9), limit: new Point('O', 156, 8), name: 16 }
    , _74: { blanche: new Point('A', 4, 8, LEGEND_POSITION.E), jaune: new Point('B', 6, 9, LEGEND_POSITION.WW), rouge: new Point('K', 16, 9, LEGEND_POSITION.EE), name: 74 }
    , _37: { blanche: new OnLinePoint(), jaune: new Point('P', 151, 7, LEGEND_POSITION.E), rouge: new Point('P', 16, 9), limit: new Point('C', 16, 9, LEGEND_POSITION.WW), lines: [new Point('P', 23, 8)], name: 37 }
    , _14: { blanche: new Point('O', 6, 8, LEGEND_POSITION.EE), jaune: new Point('I', 153, 7, LEGEND_POSITION.E), rouge: new Point('P', 16, 9), name: 14 }
    , _15: { blanche: new Point('A', 55, 7, LEGEND_POSITION.NE), jaune: new Point('P', 12, undefined, LEGEND_POSITION.N), rouge: new Point('J', 14, 7, LEGEND_POSITION.E), name: 15 }
    , _55: { blanche: new OnLinePoint(), jaune: new Point('P', 33, undefined, LEGEND_POSITION.EE), rouge: new Point('A', 23, undefined, LEGEND_POSITION.N), lines: [new Point('P', 15, 9)], name: 55 }
    , _76: { blanche: new OnLinePoint(), jaune: new Point('H', 5, 9, LEGEND_POSITION.SE), rouge: new Point('I', 55, 7, LEGEND_POSITION.WW), lines: [new Point('G', 3, 9)], name: 76 }
    , _19: { blanche: new Point('P', 152, 7, LEGEND_POSITION.NE), jaune: new Point('A', 16, 7, LEGEND_POSITION.N), rouge: new Point('M', 13, 9, LEGEND_POSITION.E), name: 19 }
    , _35: { blanche: new Point('G', 6, 9, LEGEND_POSITION.SEE), jaune: new OnLinePoint(), rouge: new Point('A', 15, undefined, LEGEND_POSITION.NE), lines: [new Point('G', 11, undefined), new Point('G', 21, 7)], name: 35 }
    , _89: { blanche: new Point('N', 151, 7, LEGEND_POSITION.E), jaune: new Point('C', 16, 8, LEGEND_POSITION.W), rouge: new Point('I', 14, undefined), name: 89 }
    , _23: { blanche: new Point('K', 156, 8, LEGEND_POSITION.W), jaune: new Point('I', 23, undefined, LEGEND_POSITION.W), rouge: new Point('O', 16, 9, LEGEND_POSITION.EE), name: 23 }
    , _59: { blanche: new Point('O', 56, 8, LEGEND_POSITION.E), jaune: new Point('P', 53, 7, LEGEND_POSITION.E), rouge: new Point('A', 55, 7, LEGEND_POSITION.SE), name: 59 }
    , _40: { blanche: new Point('O', 106, 8, LEGEND_POSITION.E), jaune: new Point('H', 123, 9, LEGEND_POSITION.E), rouge: new Point('G', 13, 8, LEGEND_POSITION.E), name: 40 }
    , _51: { blanche: new Point('P', 22, 9, LEGEND_POSITION.W), jaune: new Point('P', 104, 9, LEGEND_POSITION.E), rouge: new Point('P', 24, undefined, LEGEND_POSITION.S), name: 51 }
    , _98: { blanche: new Point('F', 11, 7, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('I', 14, undefined), lines: [new Point('G', 22, 9)], name: 98 }
    , _21: { blanche: new Point('G', 21, undefined, LEGEND_POSITION.E), jaune: new Point('G', 111, undefined, LEGEND_POSITION.E), rouge: new Point('P', 16, 9), limit: new Point('F', 102, 8, LEGEND_POSITION.E), name: 21 }
    , _29: { blanche: new Point('B', 115, undefined, LEGEND_POSITION.E), jaune: new Point('A', 32, undefined, LEGEND_POSITION.E), rouge: new Point('A', 42, 8, LEGEND_POSITION.E), name: 29 }
    , _38: { blanche: new Point('A', 13, 8, LEGEND_POSITION.E), jaune: new Point('A', 22, 9, LEGEND_POSITION.N), rouge: new Point('A', 33, 9, LEGEND_POSITION.NW), name: 38 }
    , _62: { blanche: new Point('D', 116, 8, LEGEND_POSITION.E), jaune: new Point('M', 156, 8, LEGEND_POSITION.E), rouge: new Point('O', 26, 8, LEGEND_POSITION.E), name: 62 }
    , _27: { blanche: new Point('M', 3, 9, LEGEND_POSITION.E), jaune: new Point('N', 6, 9, LEGEND_POSITION.E), rouge: new Point('A', 11, 9, LEGEND_POSITION.E), name: 27 }
    , _34: { blanche: new Point('I', 14, 7, LEGEND_POSITION.E), jaune: new Point('I', 51, undefined, LEGEND_POSITION.E), rouge: new Point('O', 21, undefined, LEGEND_POSITION.E), name: 34 }
    , _79: { blanche: new Point('A', 114, undefined, LEGEND_POSITION.EE), jaune: new Point('B', 116, 9, LEGEND_POSITION.E), rouge: new Point('B', 6, 8, LEGEND_POSITION.E), limit: new Point('B', 55, undefined, LEGEND_POSITION.E), name: 79 }
    , _64: { blanche: new Point('P', 123, 7, LEGEND_POSITION.W), jaune: new Point('F', 6, 9, LEGEND_POSITION.E), rouge: new Point('H', 13, undefined, LEGEND_POSITION.SE), name: 64 }
    , _57: { blanche: new OnLinePoint(), jaune: new Point('F', 4, undefined, LEGEND_POSITION.E), rouge: new Point('H', 15, undefined, LEGEND_POSITION.NW), lines: [new Point('E', 31, undefined)], name: 57 }
    , _96: { blanche: new OnLinePoint(), jaune: new Point('P', 156, 8, LEGEND_POSITION.E), rouge: new Point('P', 152, 7, LEGEND_POSITION.SE), lines: [new Point('P', 56, 8)], name: 96 }
    , _77: { blanche: new Point('G', 1, 7, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('I', 135, undefined, LEGEND_POSITION.E), limit: new Point('I', 113, 9, LEGEND_POSITION.NE), lines: [new Point('H', 1, 7)], name: 77 }
    , _32: { blanche: new Point('B', 121, undefined, LEGEND_POSITION.E), jaune: new Point('K', 151, 7, LEGEND_POSITION.E), rouge: new Point('K', 16, undefined, LEGEND_POSITION.W), limit: new Point('L', 125, 9, LEGEND_POSITION.E), name: 32 }
    , _87: { blanche: new Point('I', 3, 9, LEGEND_POSITION.E), jaune: new Point('P', 1, undefined, LEGEND_POSITION.E), rouge: new Point('A', 11, 9, LEGEND_POSITION.EE), name: 87 }
    , _47: { blanche: new Point('A', 53, undefined, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('P', 16, 8, LEGEND_POSITION.EE), lines: [new Point('A', 41, 7)], name: 47 }
    , _12: { blanche: new Point('I', 143, 9, LEGEND_POSITION.E), jaune: new Point('J', 135, 9, LEGEND_POSITION.E), rouge: new Point('K', 12, undefined, LEGEND_POSITION.E), name: 12 }
    , _26: { blanche: new OnLinePoint(), jaune: new Point('B', 26, 9, LEGEND_POSITION.EE), rouge: new Point('A', 35, 7, LEGEND_POSITION.E), lines: [new Point('A', 51, 9)], name: 26 }
    , _88: { blanche: new Point('C', 6, 8, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('A', 14, undefined), lines: [new Point('C', 22, 7)], name: 88 }
    , _20: { blanche: new Point('I', 24, undefined, LEGEND_POSITION.E), jaune: new Point('I', 21, 7, LEGEND_POSITION.E), rouge: new Point('M', 36, 9, LEGEND_POSITION.E), name: 20 }
    , _28: { blanche: new Point('H', 6, 8, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('A', 14, undefined), lines: [new Point('H', 12, 8)], name: 28 }
    , _91: { blanche: new Point('A', 44, 7, LEGEND_POSITION.N), jaune: new Point('M', 3, 8, LEGEND_POSITION.E), rouge: new Point('M', 23, undefined, LEGEND_POSITION.E), name: 91 }
    , _43: { blanche: new Point('C', 146, 8, LEGEND_POSITION.E), jaune: new Point('E', 3, 9, LEGEND_POSITION.E), rouge: new Point('D', 16, undefined, LEGEND_POSITION.E), name: 43 }
    , _39: { blanche: new Point('C', 146, 9, LEGEND_POSITION.E), jaune: new Point('G', 6, 8, LEGEND_POSITION.E), rouge: new Point('I', 14, undefined), name: 39 }
    , _100: { blanche: new Point('N', 6, 8, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('A', 14, undefined), limit: new Point('O', 156, 8), lines: [new Point('N', 56, 8)], name: 100 }
    , _75: { blanche: new Point('A', 44, 9, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 153, 8, LEGEND_POSITION.E), lines: [new Point('B', 14, 7)], name: 75 }
    , _11: { blanche: new Point('A', 14, undefined), jaune: new Point('B', 26, 8, LEGEND_POSITION.W), rouge: new Point('A', 1, 9, LEGEND_POSITION.E), name: 11 }
    , _73: { blanche: new Point('E', 46, 7, LEGEND_POSITION.E), jaune: new Point('H', 146, 9, LEGEND_POSITION.E), rouge: new Point('H', 111, undefined, LEGEND_POSITION.E), name: 73 }
    , _30: { blanche: new Point('C', 6, 9, LEGEND_POSITION.WW), jaune: new Point('G', 6, 9, LEGEND_POSITION.SW), rouge: new Point('H', 12, 9, LEGEND_POSITION.S), name: 30 }
}

var figures = figuresRightHanded;

var figuresLeftHanded = {
    _1: { blanche: new Point('K', 42, undefined, LEGEND_POSITION.E), jaune: new Point('K', 56, 8, LEGEND_POSITION.E), rouge: new Point('P', 13, undefined), name: 1 }
    , _2: { blanche: new Point('K', 156, 8, LEGEND_POSITION.WW), jaune: new Point('H', 1, undefined, LEGEND_POSITION.W), rouge: new Point('A', 14, undefined, LEGEND_POSITION.SS), name: 2 }
    , _3: { blanche: new Point('B', 125, undefined, LEGEND_POSITION.E), jaune: new Point('B', 13, 9, LEGEND_POSITION.SE), rouge: new Point('A', 35, undefined, LEGEND_POSITION.W), name: 3 }
    , _4: { blanche: new Point('B', 44, 7, LEGEND_POSITION.E), jaune: new Point('A', 54, 7, LEGEND_POSITION.E), rouge: new Point('B', 16, 9), name: 4 }
    , _5: { blanche: new Point('C', 6, 8, LEGEND_POSITION.SE), jaune: new Point('C', 13, 7, LEGEND_POSITION.SE), rouge: new Point('A', 34, undefined, LEGEND_POSITION.E), name: 5 }
    , _6: { blanche: new Point('P', 3, undefined, LEGEND_POSITION.E), jaune: new Point('P', 6, 9, LEGEND_POSITION.WWW), rouge: new Point('P', 16, 9, LEGEND_POSITION.W), limit: new Point('C', 5, undefined, LEGEND_POSITION.E), name: 6 }
    , _7: { blanche: new Point('H', 3, 9, LEGEND_POSITION.W), jaune: new Point('C', 6, 9, LEGEND_POSITION.SWW), rouge: new Point('H', 12, 9, LEGEND_POSITION.N), name: 7 }
    , _8: { blanche: new OnLinePoint(), jaune: new Point('I', 15, undefined, LEGEND_POSITION.S), rouge: new Point('P', 13, undefined), limit: new Point('N', 156, 8, LEGEND_POSITION.SW), lines: [new Point('I', 26, undefined)], name: 8 }
    , _9: { blanche: new Point('A', 11, 7, LEGEND_POSITION.W), jaune: new Point('A', 55, 7, LEGEND_POSITION.E), rouge: new Point('P', 113, undefined, LEGEND_POSITION.E), name: 9 }
    , _10: { blanche: new Point('G', 136, 7, LEGEND_POSITION.E), jaune: new Point('H', 22, undefined, LEGEND_POSITION.E), rouge: new Point('K', 11, undefined, LEGEND_POSITION.E), name: 10 }
    , _11: { blanche: new Point('P', 13, undefined), jaune: new Point('P', 26, 8, LEGEND_POSITION.W), rouge: new Point('P', 5, 9, LEGEND_POSITION.E), name: 11 }
    , _12: { blanche: new Point('H', 143, 9, LEGEND_POSITION.E), jaune: new Point('G', 131, 9, LEGEND_POSITION.E), rouge: new Point('F', 15, undefined, LEGEND_POSITION.E), name: 12 }
    , _13: { blanche: new Point('M', 151, 8, LEGEND_POSITION.E), jaune: new Point('D', 45, undefined, LEGEND_POSITION.E), rouge: new Point('C', 16, 9, LEGEND_POSITION.E), limit: new Point('C', 51, 8, LEGEND_POSITION.E), name: 13 }
    , _14: { blanche: new Point('C', 6, 8, LEGEND_POSITION.EE), jaune: new Point('I', 153, 7, LEGEND_POSITION.E), rouge: new Point('B', 16, 9), name: 14 }
    , _15: { blanche: new Point('P', 52, 7, LEGEND_POSITION.NE), jaune: new Point('A', 15, undefined, LEGEND_POSITION.N), rouge: new Point('G', 13, 7, LEGEND_POSITION.E), name: 15 }
    , _16: { blanche: new Point('P', 43, undefined, LEGEND_POSITION.WW), jaune: new Point('P', 46, 9, LEGEND_POSITION.NW), rouge: new Point('B', 16, 9), limit: new Point('O', 156, 8), name: 16 }
    , _17: { blanche: new Point('A', 33, 7, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('M', 13, 9, LEGEND_POSITION.E), lines: [new Point('A', 51, undefined)], name: 17 }
    , _18: { blanche: new Point('N', 22, undefined, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('H', 13, undefined), limit: new Point('E', 6, 9, LEGEND_POSITION.SE), lines: [new Point('O', 46, 8)], name: 18 }
    , _19: { blanche: new Point('P', 152, 7, LEGEND_POSITION.NE), jaune: new Point('P', 11, 7, LEGEND_POSITION.N), rouge: new Point('D', 13, 9, LEGEND_POSITION.E), name: 19 }
    , _20: { blanche: new Point('H', 23, undefined, LEGEND_POSITION.E), jaune: new Point('H', 26, 7, LEGEND_POSITION.E), rouge: new Point('E', 36, 9, LEGEND_POSITION.E), name: 20 }
    , _21: { blanche: new Point('J', 26, undefined, LEGEND_POSITION.E), jaune: new Point('J', 116, undefined, LEGEND_POSITION.E), rouge: new Point('B', 16, 9), limit: new Point('K', 104, 8, LEGEND_POSITION.E), name: 21 }
    , _22: { blanche: new Point('P', 43, undefined, LEGEND_POSITION.W), jaune: new Point('P', 46, 9, LEGEND_POSITION.SW), rouge: new Point('P', 26, 9, LEGEND_POSITION.E), name: 22 }
    , _23: { blanche: new Point('K', 156, 8, LEGEND_POSITION.W), jaune: new Point('H', 24, undefined, LEGEND_POSITION.W), rouge: new Point('C', 16, 9, LEGEND_POSITION.EE), name: 23 }
    , _24: { blanche: new Point('B', 111, undefined, LEGEND_POSITION.W), jaune: new Point('J', 23, 9, LEGEND_POSITION.E), rouge: new Point('A', 13, 9, LEGEND_POSITION.S), name: 24 }
    , _25: { blanche: new OnLinePoint(), jaune: new Point('I', 156, 7, LEGEND_POSITION.NE), rouge: new Point('I', 155, 7, LEGEND_POSITION.S), lines: [new Point('I', 23, 8), new Point('I', 124, 9)], name: 25 }
    , _26: { blanche: new OnLinePoint(), jaune: new Point('P', 26, 9, LEGEND_POSITION.EE), rouge: new Point('P', 32, 7, LEGEND_POSITION.E), lines: [new Point('P', 55, 9)], name: 26 }
    , _27: { blanche: new Point('D', 3, 9, LEGEND_POSITION.E), jaune: new Point('D', 6, 9, LEGEND_POSITION.E), rouge: new Point('P', 15, 9, LEGEND_POSITION.E), name: 27 }
    , _28: { blanche: new Point('J', 6, 8, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('P', 13, undefined), lines: [new Point('I', 14, 8)], name: 28 }
    , _29: { blanche: new Point('O', 112, undefined, LEGEND_POSITION.E), jaune: new Point('P', 35, undefined, LEGEND_POSITION.E), rouge: new Point('P', 44, 8, LEGEND_POSITION.E), name: 29 }
    , _30: { blanche: new Point('O', 6, 9, LEGEND_POSITION.WW), jaune: new Point('K', 6, 9, LEGEND_POSITION.SW), rouge: new Point('I', 14, 9, LEGEND_POSITION.S), name: 30 }
    , _31: { blanche: new Point('O', 13, 8, LEGEND_POSITION.E), jaune: new Point('O', 56, 8, LEGEND_POSITION.E), rouge: new Point('O', 54, 8, LEGEND_POSITION.E), name: 31 }
    , _32: { blanche: new Point('O', 126, undefined, LEGEND_POSITION.E), jaune: new Point('K', 151, 7, LEGEND_POSITION.E), rouge: new Point('F', 11, undefined, LEGEND_POSITION.W), limit: new Point('E', 121, 9, LEGEND_POSITION.E), name: 32 }
    , _33: { blanche: new Point('B', 51, 7, LEGEND_POSITION.W), jaune: new Point('D', 126, 9, LEGEND_POSITION.E), rouge: new Point('A', 14, undefined, LEGEND_POSITION.S), name: 33 }
    , _34: { blanche: new Point('P', 13, 7, LEGEND_POSITION.E), jaune: new Point('P', 56, undefined, LEGEND_POSITION.E), rouge: new Point('J', 26, undefined, LEGEND_POSITION.E), name: 34 }
    , _35: { blanche: new Point('K', 6, 9, LEGEND_POSITION.SEE), jaune: new OnLinePoint(), rouge: new Point('P', 12, undefined, LEGEND_POSITION.NE), lines: [new Point('J', 16, undefined), new Point('J', 26, 7)], name: 35 }
    , _36: { blanche: new OnLinePoint(), jaune: new Point('L', 156, 8, LEGEND_POSITION.NW), rouge: new Point('H', 12, undefined, LEGEND_POSITION.E), lines: [new Point('F', 56, 8)], name: 36 }
    , _37: { blanche: new OnLinePoint(), jaune: new Point('P', 151, 7, LEGEND_POSITION.E), rouge: new Point('B', 16, 9), limit: new Point('O', 16, 9, LEGEND_POSITION.WW), lines: [new Point('A', 23, 8)], name: 37 }
    , _38: { blanche: new Point('P', 13, 8, LEGEND_POSITION.E), jaune: new Point('P', 24, 9, LEGEND_POSITION.N), rouge: new Point('P', 33, 9, LEGEND_POSITION.NW), name: 38 }
    , _39: { blanche: new Point('O', 146, 9, LEGEND_POSITION.E), jaune: new Point('K', 6, 8, LEGEND_POSITION.E), rouge: new Point('H', 13, undefined), name: 39 }
    , _40: { blanche: new Point('C', 106, 8, LEGEND_POSITION.E), jaune: new Point('I', 123, 9, LEGEND_POSITION.E), rouge: new Point('J', 13, 8, LEGEND_POSITION.E), name: 40 }
    , _41: { blanche: new Point('J', 151, 7, LEGEND_POSITION.E), jaune: new Point('L', 26, 9, LEGEND_POSITION.E), rouge: new Point('P', 33, undefined, LEGEND_POSITION.NE), name: 41 }
    , _42: { blanche: new Point('L', 152, 8, LEGEND_POSITION.E), jaune: new Point('E', 16, 9, LEGEND_POSITION.E), rouge: new Point('K', 16, 9, LEGEND_POSITION.E), name: 42 }
    , _43: { blanche: new Point('O', 146, 8, LEGEND_POSITION.E), jaune: new Point('L', 3, 9, LEGEND_POSITION.E), rouge: new Point('M', 11, undefined, LEGEND_POSITION.E), name: 43 }
    , _44: { blanche: new Point('A', 123, 8, LEGEND_POSITION.E), jaune: new Point('P', 12, undefined, LEGEND_POSITION.SE), rouge: new Point('A', 15, 9, LEGEND_POSITION.W), name: 44 }
    , _45: { blanche: new Point('A', 24, 8, LEGEND_POSITION.W), jaune: new Point('A', 103, 7, LEGEND_POSITION.E), rouge: new Point('O', 11, 7, LEGEND_POSITION.W), name: 45 }
    , _46: { blanche: new Point('C', 146, 9, LEGEND_POSITION.W), jaune: new Point('I', 156, 7, LEGEND_POSITION.SE), rouge: new Point('A', 14, undefined, LEGEND_POSITION.W), name: 46 }
    , _47: { blanche: new Point('P', 54, undefined, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('B', 16, 8, LEGEND_POSITION.EE), lines: [new Point('P', 46, 7)], name: 47 }
    , _48: { blanche: new OnLinePoint(), jaune: new Point('H', 113, 9, LEGEND_POSITION.W), rouge: new Point('B', 16, 9), lines: [new Point('I', 153, 7)], name: 48 }
    , _49: { blanche: new Point('B', 114, undefined, LEGEND_POSITION.E), jaune: new Point('G', 112, 9, LEGEND_POSITION.E), rouge: new Point('P', 13, undefined), name: 49 }
    , _50: { blanche: new Point('B', 124, undefined, LEGEND_POSITION.E), jaune: new Point('O', 156, 8), rouge: new Point('I', 54, 7, LEGEND_POSITION.E), name: 50 }
    
    , _51: { blanche: new Point('A', 24, 9, LEGEND_POSITION.W), jaune: new Point('A', 102, 9, LEGEND_POSITION.E), rouge: new Point('A', 23, undefined, LEGEND_POSITION.S), name: 51 }
    , _52: { blanche: new Point('C', 13, 9, LEGEND_POSITION.E), jaune: new Point('C', 26, 9, LEGEND_POSITION.E), rouge: new Point('G', 22, 9, LEGEND_POSITION.E), name: 52 }
    , _53: { blanche: new Point('K', 32, undefined, LEGEND_POSITION.E), jaune: new Point('J', 32, 8, LEGEND_POSITION.E), rouge: new Point('I', 14, undefined, LEGEND_POSITION.NE), limit: new Point('K', 6, 8, LEGEND_POSITION.E), name: 53 }
    , _54: { blanche: new Point('C', 116, 9, LEGEND_POSITION.E), jaune: new Point('E', 145, 7, LEGEND_POSITION.E), rouge: new Point('I', 14, undefined, LEGEND_POSITION.NEE), name: 54 }
    , _55: { blanche: new OnLinePoint(), jaune: new Point('A', 34, undefined, LEGEND_POSITION.EE), rouge: new Point('P', 24, undefined, LEGEND_POSITION.N), lines: [new Point('A', 11, 9)], name: 55 }
    , _56: { blanche: new OnLinePoint(), jaune: new Point('O', 6, 9, LEGEND_POSITION.W), rouge: new Point('J', 24, 9, LEGEND_POSITION.NW), limit: new Point('O', 16, 9, LEGEND_POSITION.W), lines: [new Point('O', 4, 9), new Point('O', 14, 9)], name: 56 }
    , _57: { blanche: new OnLinePoint(), jaune: new Point('K', 3, undefined, LEGEND_POSITION.E), rouge: new Point('I', 12, undefined, LEGEND_POSITION.NW), lines: [new Point('L', 36, undefined)], name: 57 }
    , _58: { blanche: new Point('P', 23, 9, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('F', 13, 9, LEGEND_POSITION.E), lines: [new Point('P', 3, 9)], name: 58 }
    , _59: { blanche: new Point('C', 56, 8, LEGEND_POSITION.E), jaune: new Point('A', 54, 7, LEGEND_POSITION.E), rouge: new Point('P', 52, 7, LEGEND_POSITION.SE), name: 59 }
    , _60: { blanche: new Point('O', 126, 8, LEGEND_POSITION.E), jaune: new Point('P', 22, undefined, LEGEND_POSITION.NW), rouge: new Point('O', 42, undefined, LEGEND_POSITION.E), name: 60 }
    
    , _61: { blanche: new Point('A', 24, 7, LEGEND_POSITION.N), jaune: new Point('B', 36, 8, LEGEND_POSITION.E), rouge: new Point('C', 6, 8, LEGEND_POSITION.E), name: 61 }
    , _62: { blanche: new Point('N', 116, 8, LEGEND_POSITION.E), jaune: new Point('M', 156, 8, LEGEND_POSITION.E), rouge: new Point('C', 26, 8, LEGEND_POSITION.E), name: 62 }
    , _63: { blanche: new Point('B', 126, undefined, LEGEND_POSITION.E), jaune: new Point('O', 156, 8), rouge: new Point('O', 16, 9, LEGEND_POSITION.E), name: 63 }
    , _64: { blanche: new Point('A', 124, 7, LEGEND_POSITION.W), jaune: new Point('L', 6, 9, LEGEND_POSITION.E), rouge: new Point('I', 14, undefined, LEGEND_POSITION.SE), name: 64 }
    , _65: { blanche: new Point('P', 23, 9, LEGEND_POSITION.S), jaune: new OnLinePoint(), rouge: new Point('E', 16, 9, LEGEND_POSITION.E), lines: [new Point('P', 54, 9)], name: 65 }
    , _66: { blanche: new OnLinePoint(), jaune: new Point('N', 154, 8, LEGEND_POSITION.S), rouge: new Point('H', 13, undefined), lines: [new Point('O', 156, 8)], name: 66 }
    , _67: { blanche: new Point('I', 55, 8, LEGEND_POSITION.E), jaune: new Point('K', 6, 9, LEGEND_POSITION.SE), rouge: new Point('K', 16, 8, LEGEND_POSITION.W), name: 67 }
    , _68: { blanche: new Point('B', 6, 8, LEGEND_POSITION.E), jaune: new Point('J', 6, 9, LEGEND_POSITION.SE), rouge: new Point('J', 16, 8, LEGEND_POSITION.E), limit: new Point('O', 52, 8, LEGEND_POSITION.E), name: 68 }
    , _69: { blanche: new Point('B', 1, 8, LEGEND_POSITION.E), jaune: new Point('C', 13, 7, LEGEND_POSITION.E), rouge: new Point('B', 16, 8, LEGEND_POSITION.E), name: 69 }
    , _70: { blanche: new Point('K', 133, 7, LEGEND_POSITION.E), jaune: new Point('G', 16, 9, LEGEND_POSITION.E), rouge: new Point('A', 34, 9, LEGEND_POSITION.N), limit: new Point('D', 35, 7, LEGEND_POSITION.E), name: 70 }
    
    , _71: { blanche: new Point('P', 33, 9, LEGEND_POSITION.SW), jaune: new OnLinePoint(), rouge: new Point('P', 22, undefined), lines: [new Point('P', 3, 9)], name: 71 }
    , _72: { blanche: new Point('I', 24, undefined, LEGEND_POSITION.W), jaune: new OnLinePoint(), rouge: new Point('P', 33, undefined, LEGEND_POSITION.SE), lines: [new Point('I', 156, 7)], name: 72 }
    , _73: { blanche: new Point('L', 41, 7, LEGEND_POSITION.E), jaune: new Point('J', 146, 9, LEGEND_POSITION.E), rouge: new Point('I', 116, undefined, LEGEND_POSITION.E), name: 73 }
    , _74: { blanche: new Point('P', 2, 8, LEGEND_POSITION.E), jaune: new Point('P', 6, 9, LEGEND_POSITION.WW), rouge: new Point('G', 16, 9, LEGEND_POSITION.EE), name: 74 }
    , _75: { blanche: new Point('P', 42, 9, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 153, 8, LEGEND_POSITION.E), lines: [new Point('O', 13, 7)], name: 75 }
    , _76: { blanche: new OnLinePoint(), jaune: new Point('I', 1, 9, LEGEND_POSITION.SE), rouge: new Point('H', 52, 7, LEGEND_POSITION.WW), lines: [new Point('J', 3, 9)], name: 76 }
    , _77: { blanche: new Point('J', 6, 7, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('H', 132, undefined, LEGEND_POSITION.E), limit: new Point('H', 113, 9, LEGEND_POSITION.NE), lines: [new Point('I', 6, 7)], name: 77 }
    , _78: { blanche: new OnLinePoint(), jaune: new Point('H', 113, 9, LEGEND_POSITION.SE), rouge: new Point('I', 12, undefined, LEGEND_POSITION.NE), lines: [new Point('I', 153, 7)], name: 78 }
    , _79: { blanche: new Point('P', 113, undefined, LEGEND_POSITION.EE), jaune: new Point('P', 116, 9, LEGEND_POSITION.E), rouge: new Point('P', 6, 8, LEGEND_POSITION.E), limit: new Point('O', 52, undefined, LEGEND_POSITION.E), name: 79 }
    , _80: { blanche: new Point('B', 32, 9, LEGEND_POSITION.E), jaune: new Point('A', 34, undefined, LEGEND_POSITION.N), rouge: new Point('P', 15, 8, LEGEND_POSITION.E), name: 80 }

    , _81: { blanche: new Point('M', 36, 9, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('A', 13, 9, LEGEND_POSITION.SS), limit: new Point('I', 156, 7, LEGEND_POSITION.SW), lines: [new Point('M', 144, undefined)], name: 81 }
    , _82: { blanche: new Point('A', 145, 7, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('A', 13, undefined, LEGEND_POSITION.E), limit: new Point('E', 6, 9, LEGEND_POSITION.E), lines: [new Point('A', 55, undefined)], name: 82 }
    , _83: { blanche: new Point('B', 13, 9, LEGEND_POSITION.E), jaune: new Point('C', 26, 9, LEGEND_POSITION.E), rouge: new Point('P', 43, 8, LEGEND_POSITION.N), name: 83 }
    , _84: { blanche: new Point('K', 2, 8, LEGEND_POSITION.N), jaune: new Point('K', 16, 9, LEGEND_POSITION.EE), rouge: new Point('H', 13, undefined), limit: new Point('F', 6, 9, LEGEND_POSITION.E), name: 84 }
    , _85: { blanche: new Point('P', 3, undefined, LEGEND_POSITION.EE), jaune: new Point('P', 6, 9, LEGEND_POSITION.W), rouge: new Point('P', 16, 9, LEGEND_POSITION.WW), name: 85 }
    , _86: { blanche: new Point('K', 156, 8), jaune: new Point('H', 1, undefined, LEGEND_POSITION.N), rouge: new Point('B', 24, 9, LEGEND_POSITION.NE), name: 86 }
    , _87: { blanche: new Point('H', 3, 9, LEGEND_POSITION.E), jaune: new Point('A', 6, undefined, LEGEND_POSITION.E), rouge: new Point('P', 15, 9, LEGEND_POSITION.EE), name: 87 }
    , _88: { blanche: new Point('O', 6, 8, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 13, undefined), lines: [new Point('N', 25, 7)], name: 88 }
    , _89: { blanche: new Point('N', 151, 7, LEGEND_POSITION.E), jaune: new Point('O', 16, 8, LEGEND_POSITION.W), rouge: new Point('H', 13, undefined), name: 89 }
    , _90: { blanche: new Point('K', 42, 7, LEGEND_POSITION.E), jaune: new Point('I', 124, undefined, LEGEND_POSITION.N), rouge: new Point('H', 52, 7, LEGEND_POSITION.W), name: 90 }
    
    , _91: { blanche: new Point('P', 43, 7, LEGEND_POSITION.N), jaune: new Point('D', 3, 8, LEGEND_POSITION.E), rouge: new Point('D', 24, undefined, LEGEND_POSITION.E), name: 91 }
    , _92: { blanche: new Point('A', 23, 9, LEGEND_POSITION.S), jaune: new OnLinePoint(), rouge: new Point('P', 13, undefined), limit: new Point('N', 55, 9, LEGEND_POSITION.E), lines: [new Point('A', 21, undefined)], name: 92 }
    , _93: { blanche: new Point('M', 153, 7, LEGEND_POSITION.E), jaune: new Point('D', 45, undefined, LEGEND_POSITION.EE), rouge: new Point('H', 13, undefined), limit: new Point('B', 54, 7, LEGEND_POSITION.E), name: 93 }
    , _94: { blanche: new Point('O', 42, 7, LEGEND_POSITION.E), jaune: new Point('O', 146, 9, LEGEND_POSITION.EE), rouge: new Point('I', 14, undefined, LEGEND_POSITION.SEE), name: 94 }
    , _95: { blanche: new Point('H', 51, 8, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('B', 16, 9), lines: [new Point('H', 11, 8)], name: 95 }
    , _96: { blanche: new OnLinePoint(), jaune: new Point('P', 156, 8, LEGEND_POSITION.E), rouge: new Point('P', 152, 7, LEGEND_POSITION.SE), lines: [new Point('B', 56, 8)], name: 96 }
    , _97: { blanche: new Point('H', 32, undefined, LEGEND_POSITION.N), jaune: new OnLinePoint(), rouge: new Point('K', 15, 7, LEGEND_POSITION.E), limit: new Point('H', 56, undefined, LEGEND_POSITION.EE), lines: [new Point('H', 5, undefined)], name: 97 }
    , _98: { blanche: new Point('K', 16, 7, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('H', 13, undefined), lines: [new Point('J', 24, 9)], name: 98 }
    , _99: { blanche: new OnLinePoint(), jaune: new Point('A', 24, 7, LEGEND_POSITION.S), rouge: new Point('A', 15, undefined, LEGEND_POSITION.S), lines: [new Point('A', 26, 7)], name: 99 }
    , _100: { blanche: new Point('D', 6, 8, LEGEND_POSITION.E), jaune: new OnLinePoint(), rouge: new Point('P', 13, undefined), limit: new Point('O', 156, 8), lines: [new Point('D', 56, 8)], name: 100 }
}

var sets = {
    "A":[85, 49, 35, 10, 37, 16, 82],
    "B":[51,  8, 19, 78, 94, 96, 32],
    "C":[ 3, 26, 20, 24, 57,  6, 92],
    "D":[69,  4, 99, 36,  2, 87, 34],
    "E":[21, 74, 55, 50, 27, 52, 88],
    "F":[11, 54, 47, 100,89, 76, 84],
    "G":[79, 18, 23, 66, 29, 90, 72],
    "H":[61, 63, 95, 42, 67, 17, 44],
    "I":[33, 40, 48, 71,  5, 14, 60],
    "J":[93, 45, 15, 56,  9, 77, 28],
    "K":[ 1, 70, 81, 91, 83, 43, 62],
    "L":[73, 65, 22, 30,  7, 39, 68],
    "M":[31, 80, 38, 98, 86, 46, 64],
    "N":[13, 25, 97, 12, 59, 53, 58]
};

var figuresCoeff = {
    61: 5,
    54: 8,
    95: 7,
    78: 10,
    9: 6,
    71: 9,
    93: 5,
    53: 8,
    4: 7,
    68: 10,
    41: 5,
    6: 8,
    63: 7,
    72: 10,
    2: 6,
    50: 9,
    69: 5,
    18: 8,
    94: 7,
    66: 10,
    3: 5,
    49: 8,
    97: 7,
    44: 10,
    67: 6,
    24: 9,
    83: 5,
    46: 8,
    25: 7,
    92: 10,
    33: 5,
    45: 8,
    65: 7,
    82: 10,
    86: 6,
    10: 9,
    5: 5,
    75: 8,
    29: 7,
    58: 10,
    1: 5,
    52: 8,
    38: 7,
    62: 10,
    99: 6,
    42: 9,
    27: 5,
    76: 8,
    15: 7,
    34: 10,
    79: 5,
    8: 8,
    80: 7,
    64: 10,
    57: 6,
    96: 9,
    11: 5,
    77: 8,
    19: 7,
    32: 10,
    73: 5,
    87: 8,
    35: 7,
    30: 10,
    47: 6,
    12: 9,
    89: 5,
    26: 8,
    23: 7,
    88: 10,
    59: 5,
    40: 8,
    20: 7,
    28: 10,
    55: 6,
    91: 9,
    51: 5,
    43: 8,
    70: 7,
    98: 10,
    21: 5,
    39: 8,
    81: 7,
    60: 10,
    7: 6,
    56: 9,
    31: 5,
    90: 8,
    48: 7,
    84: 10,
    13: 5,
    16: 8,
    74: 7,
    36: 10,
    37: 6,
    14: 9,
    85: 5,
    17: 8,
    22: 7,
    100: 10
}